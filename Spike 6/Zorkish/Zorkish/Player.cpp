#include <iostream>
#include <algorithm> 
#include <string>
#include <future>

using namespace std;

class Item
{
public:
	std::string name;
	std::string desc;
};

class Inventory
{
public:
	void printContents()
	{
		std::cout << "|#|    ITEM NAME    " << endl;
		std::cout << "|-|-----------------" << endl;
		for (unsigned i = 0; i < inventory.size(); i++)
		{
			std::cout << "|" << i << "|    " << inventory[i].name << endl;
		}
		std::cout << "|-|-----------------" << endl << endl;
	}

	int indexByName(std::string itemName)
	{
		for (unsigned i = 0; i < inventory.size(); i++)
		{
			if (inventory[i].name == itemName)
			{
				return i;
			}
		}
		return -1;
	}

	void addItem(Item item)
	{
		inventory.push_back(item);
	}

	void removeItem(std::string itemName)
	{
		int itemIndex = indexByName(itemName);
		inventory.erase(inventory.begin() + itemIndex);
	}
private:
	std::vector<Item> inventory;
};

class Player
{
public:
	//TODO
};

int inventoryTest()
{
	Inventory inventory;

	Item testItem;
	testItem.name = "Sword";
	testItem.desc = "This is a steel sword";
	inventory.addItem(testItem);

	inventory.printContents();

	testItem.name = "Shield";
	testItem.desc = "This is a wooden shield";
	inventory.addItem(testItem);

	inventory.printContents();

	inventory.removeItem("Sword");

	inventory.printContents();

	return 0;
}