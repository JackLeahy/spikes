#ifndef PLAYER_H    // To make sure you don't declare the function more than once by including the header multiple times.
#define PLAYER_H

#include <iostream>
#include <algorithm> 
#include <string>
#include <future>

using namespace std;

int inventoryTest();

class Item
{
public:
	std::string name;
	std::string desc;
};

class Inventory
{
public:
	void printContents()
	{
		std::cout << "|#|    ITEM NAME    " << endl;
		std::cout << "|-|-----------------" << endl;
		for (unsigned i = 0; i < inventory.size(); i++)
		{
			std::cout << "|" << i << "|    " << inventory[i].name << endl;
		}
		std::cout << "|-|-----------------" << endl << endl;
	}

	int indexByName(std::string itemName)
	{
		for (unsigned i = 0; i < inventory.size(); i++)
		{
			if (inventory[i].name == itemName)
			{
				return i;
			}
		}
		return -1;
	}

	void addItem(Item item)
	{
		inventory.push_back(item);
	}

	void removeItem(std::string itemName)
	{
		int itemIndex = indexByName(itemName);
		inventory.erase(inventory.begin() + itemIndex);
	}
	int inventoryTest()
	{
		Item testItem;
		testItem.name = "Sword";
		testItem.desc = "This is a steel sword";
		this->addItem(testItem);

		this->printContents();

		testItem.name = "Shield";
		testItem.desc = "This is a wooden shield";
		this->addItem(testItem);

		this->printContents();

		this->removeItem("Sword");

		this->printContents();

		return 0;
	}
	std::vector<Item> inventory;
	int inventoryLimit = 10;
};



#endif