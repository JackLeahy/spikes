#include <iostream>
#include <algorithm> 
#include <string>
#include <future>

#include "Player.h"

using namespace std;

class Player
{
public:
	//TODO
};

int inventoryTest()
{
	Inventory inventory;

	Item testItem;
	testItem.name = "Sword";
	testItem.desc = "This is a steel sword";
	inventory.addItem(testItem);

	inventory.printContents();

	testItem.name = "Shield";
	testItem.desc = "This is a wooden shield";
	inventory.addItem(testItem);

	inventory.printContents();

	inventory.removeItem("Sword");

	inventory.printContents();

	return 0;
}

