#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Zorkish/Player.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitSeries2
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(InventoryCount)
		{
			Inventory iv = Inventory();
			iv.inventoryTest();

			Assert::IsTrue(iv.inventory.size() == 1);
		}

		TEST_METHOD(InventoryLimit)
		{
			Inventory iv = Inventory();
			iv.inventoryTest();

			Assert::IsTrue(iv.inventory.size() < iv.inventoryLimit);
		}

	};
}