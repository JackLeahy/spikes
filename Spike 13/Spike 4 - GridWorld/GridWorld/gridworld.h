#ifndef GRIDWORLD_H
#define GRIDWORLD_H

#include <iostream>
#include <algorithm> 
#include <string>
#include <thread>
#include <future>

using namespace std;

const string BLANK = " ";
const string CHAR = "S";
const string WALL = "#";
const string GOAL = "G";
const string DEATH = "D";

const int MAP_WIDTH = 8;
const int MAP_HEIGHT = 8;
const int START_ROW = 7;
const int START_COLUMN = 2;

class Coordinate
{
public:
	int row;
	int column;
};

class Map
{
public:
	string map[8][8] =
	{
		{ "#","#","#","#","#","#","#","#" },
		{ "#","G"," ","D","#","D"," ","#" },
		{ "#"," "," "," ","#"," "," ","#" },
		{ "#","#","#"," ","#"," ","D","#" },
		{ "#"," "," "," ","#"," "," ","#" },
		{ "#"," ","#","#","#","#"," ","#" },
		{ "#"," "," "," "," "," "," ","#" },
		{ "#","#","S","#","#","#","#","#" }
	};
	Coordinate playerPos = Coordinate();
	void movePlayer(string dir);
	string checkEndCondition();
};

#endif