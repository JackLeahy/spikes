#include "stdafx.h"
#include "CppUnitTest.h"
#include "../GridWorld/gridworld.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace TestSeries1
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(CheckStartColumn)
		{
			Assert::IsTrue(START_COLUMN > 0 && START_COLUMN < MAP_WIDTH);
		}
		TEST_METHOD(CheckStartRow)
		{
			Assert::IsTrue(START_ROW > 0 && START_ROW < MAP_HEIGHT);
		}
		TEST_METHOD(CheckGoalPosition)
		{
			Map map = Map();
			Assert::IsTrue(map.map[1][1] == "G");
		}
		TEST_METHOD(CheckMapHeight)
		{
			Map map = Map();
			Assert::IsTrue(sizeof(map.map) / sizeof(*map.map) == MAP_HEIGHT);
		}
		TEST_METHOD(CheckMapWidth)
		{
			Map map = Map();
			Assert::IsTrue(sizeof(map.map[0]) / sizeof(*map.map[0]) == MAP_WIDTH);
		}
	};
}