#include <algorithm> 
#include <string>
#include <thread>
#include <future>
#include <sstream>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <map>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "Room.h"
#include "Adventure.h"

using namespace std;
using namespace rapidjson;

const char* parseFile(std::string path)
{
	std::ifstream in(path);
	std::string contents((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());

	const char* json = contents.c_str();
	cout << json;

	return json;
}

Adventure::Adventure(std::string adventureName)
{
	std::string path;
	if (adventureName == "Adventure 1")
	{
		path = "data/ForestCaveAdventure.json";
	}
	else if (adventureName == "Adventure 2")
	{
		path = "data/testadventuremap.json";
	}
	loadAdventure(path);
	movePlayerToRoom(this->startRoom);
}

void Adventure::loadAdventure(std::string path)
{
	std::ifstream in(path);
	std::string contents((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());

	const char* json = contents.c_str();

	Document document;
	document.Parse(json);

	const Value& jsonRooms = document["rooms"];

	for (SizeType i = 0; i < jsonRooms.Size(); i++)
	{
		Room room = Room(jsonRooms[i]);
		rooms.push_back(room);
	}

	const Value& jsonStart = document["startRoom"];
	startRoom = jsonStart.GetInt();
}

void Adventure::pickupItem(std::string itemName)
{
	for (unsigned i = 0; i < rooms[player.currentRoomId].items.size(); i++)
	{
		if (rooms[player.currentRoomId].items[i].name == itemName)
		{
			player.pickupItem(rooms[player.currentRoomId].items[i]);
			rooms[player.currentRoomId].items.erase(rooms[player.currentRoomId].items.begin() + i);
			break;
		}
	}
}

void Adventure::dropItem(std::string itemName)
{
	player.dropItem(itemName);
}

void Adventure::printInventory()
{
	if (player.inventory.size() > 1)
	{
		std::cout << "   --|INVENTORY|--  " << endl;
		for (unsigned i = 0; i < player.inventory.size(); i++)
		{
			std::cout << player.inventory[i].name << endl;
		}
	}
	else
	{
		std::cout << "Your inventory is empty." << endl;
	}
}

void Adventure::movePlayerInDir(std::string dir)
{
	if (rooms[player.currentRoomId].hasConnectionInDir(dir))
	{
		movePlayerToRoom(rooms[player.currentRoomId].getRoomConnection(dir));
	}
}

void Adventure::movePlayerToRoom(int roomId)
{
	player.move(roomId);
	rooms[player.currentRoomId].printDesc();
}