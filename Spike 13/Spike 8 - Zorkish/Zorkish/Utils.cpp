#include <iostream>
#include <algorithm> 
#include <string>
#include <future>

using namespace std;

void lineBreak()
{
	std::cout << "----------------------------------------------------------" << endl;
}

std::string getLine()
{
	std::string input;

	getline(cin, input);
	std::transform(input.begin(), input.end(), input.begin(), ::toupper);

	return input;
}