#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <iostream>

using namespace std;

void lineBreak();

std::string getLine();

#endif