#ifndef ADVENTURE_H 
#define ADVENTURE_H

#include "Player.h"
#include "Room.h"

using namespace std;

class Adventure
{
public:
	Adventure(std::string adventureName);
	virtual void loadAdventure(std::string path);
	virtual void pickupItem(std::string itemName);
	virtual void dropItem(std::string itemName);
	virtual void printInventory();
	virtual void movePlayerInDir(std::string dir);
	virtual void movePlayerToRoom(int roomId);

	Player player;
	int startRoom;
	std::vector<Room> rooms;
};

#endif