#ifndef COMMANDER_H
#define COMMANDER_H

class Action
{
public:
	Action();
	std::string target;
	std::string entity;
};

class Command
{
public:
	virtual void execute(Adventure *adventure, Action action) = 0;
};

class Move : public Command
{
public:
	void execute(Adventure *adventure, Action action);
};

class Pickup : public Command
{
	void execute(Adventure *adventure, Action action);
};

class Drop : public Command
{
	void execute(Adventure *adventure, Action action);
};

class Inventory : public Command
{
public:
	void execute(Adventure *adventure, Action action);
};

class CommandManager
{
public:
	CommandManager();
	//CommandManager(Adventure *adventure);
	void recieveCommand(std::string input);
	bool checkDictionary(std::string input);
	std::map<std::string, std::string> dictionary;
private:
	Adventure *adventure;
	
};

#endif
