#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Zorkish/Commander.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace TestSeries3
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(CheckDictionaryLoaded)
		{
			CommandManager cm = CommandManager();
			Assert::IsTrue(cm.dictionary.size() > 0);
		}

	};
}