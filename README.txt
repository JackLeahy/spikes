JACK LEAHY - 9990119
Games Programming - Sem 2 - 2016

======================================================
SUBMISSION DIRECTORY
------------------------------------------------------
Softcopy of all submission pieces for the portfolio.
======================================================

======================================================
SPIKES DIRECTORY
------------------------------------------------------
All Spike Reports
All Spike Source Code
======================================================

======================================================
DISTINCTION PROJECT DIRECTORY
------------------------------------------------------
Source code for my distinction project (VS C++ project)
======================================================

To run any C++ Spike open the project solution and run from within Visual Studio,

If there are no dependencies (data folder),
navigate to ../<ProjectName>/<ProjectName>/Debug/<ProjectName>.exe

To run the Distinction Project, navigate to ../DistinctionProject/Runnable/NewZorkish.exe

======================================================
LICENSE
------------------------------------------------------
Use, copy, destroy, and/or sell my code all you want.
I really don't care.
======================================================