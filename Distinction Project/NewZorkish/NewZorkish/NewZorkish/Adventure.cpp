#include <algorithm> 
#include <string>
#include <thread>
#include <future>
#include <sstream>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <map>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "Adventure.h"

using namespace std;
using namespace rapidjson;

const char* parseFile(std::string path)
{
	std::ifstream in(path);
	std::string contents((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());

	const char* json = contents.c_str();
	cout << json;

	return json;
}

Adventure::Adventure(std::string adventureName, EntityFactory *entityFactory)
{
	this->entityFactory = entityFactory;
	this->roomContainer = new Container();

	std::string path;
	if (adventureName == "Adventure 1")
	{
		path = "data/NewForestCaveAdventure.json";
	}
	else if (adventureName == "Adventure 2")
	{
		path = "data/testadventuremap.json";
	}

	loadAdventure(path);
	loadPlayer();
}

Adventure::~Adventure()
{
	delete roomContainer;
	delete player->getComponent("PLAYER");
	delete player->getComponent("CONTAINER");
	delete player;
}

void Adventure::loadAdventure(std::string path)
{
	std::ifstream in(path);
	std::string contents((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());

	const char* json = contents.c_str();

	Document document;
	document.Parse(json);

	const Value& jsonRooms = document["rooms"];

	for (SizeType i = 0; i < jsonRooms.Size(); i++)
	{
		roomContainer->addEntity(entityFactory->constructEntity(jsonRooms[i]));
	}
	startRoom = document["startRoom"].GetString();
}

void Adventure::loadPlayer()
{
	player = new Entity();

	Player* playerC = new Player();
	playerC->move(this->startRoom);
	player->addComponent("PLAYER", playerC);

	Container* containerC = new Container();
	player->addComponent("CONTAINER", containerC);

	Entity* currentRoom = roomContainer->getEntityByName(playerC->currentRoomId);
	Container* roomContainer = (Container *)currentRoom->getComponent("CONTAINER");

	std::cout << "  --  " << currentRoom->name << "  --  " << endl;
	std::cout << currentRoom->desc << endl;
	roomContainer->printContents("");
}