#include <algorithm> 
#include <string>
#include <future>
#include <sstream>
#include <iostream>
#include <fstream>
#include <map>

#include "Adventure.h"
#include "Commander.h"
#include "Utils.h"

using namespace std;

void moveObstacle(Entity *targetEntity, Adventure *adventure)
{
	if (targetEntity->hasComponent("OBSTACLE"))
	{
		Obstacle *obstacle = (Obstacle *)targetEntity->getComponent("OBSTACLE");
		Player* playerC = (Player *)adventure->player->getComponent("PLAYER");
		Entity* currentRoom = adventure->roomContainer->getEntityByName(playerC->currentRoomId);
		Room* roomC = (Room *)currentRoom->getComponent("ROOM");

		roomC->addConnection(obstacle->dir, obstacle->connection);

		targetEntity->removeComponent("OBSTACLE");

		std::cout << "A passage is revealed from removing the " << targetEntity->name << "." << endl;
	}
}

void moveEntity(Entity *sourceEntity, Entity *targetEntity, Adventure *adventure, Action action, std::string message)
{
	Container* sourceContainer;
	Container* targetContainer;
	Entity* entity;

	//Validation of container and locked status
	if (!sourceEntity->hasComponent("CONTAINER"))
	{
		std::cout << "The " << sourceEntity->desc << " cannot contain objects." << endl;
		return;
	}
	if (!targetEntity->hasComponent("CONTAINER"))
	{
		std::cout << "The " << targetEntity->desc << " cannot contain objects." << endl;
		return;
	}
	if (sourceEntity->hasComponent("LOCKABLE"))
	{
		Lockable *lockable = (Lockable *)sourceEntity->getComponent("LOCKABLE");

		if (lockable->isLocked)
		{
			std::cout << "The " << sourceEntity->name << " is locked shut." << endl;
			return;
		}
	}
	if (targetEntity->hasComponent("LOCKABLE"))
	{
		Lockable *lockable = (Lockable *)targetEntity->getComponent("LOCKABLE");

		if (lockable->isLocked)
		{
			std::cout << "The " << targetEntity->name << " is locked shut." << endl;
			return;
		}
	}

	sourceContainer = (Container *)sourceEntity->getComponent("CONTAINER");
	targetContainer = (Container *)targetEntity->getComponent("CONTAINER");

	if (sourceContainer->containsEntity(action.entity))
	{
		entity = sourceContainer->getEntityByName(action.entity);
		
		if (action.container != "INVENTORY" || (entity->hasComponent("CARRYABLE") && action.container == "INVENTORY"))
		{
			targetContainer->addEntity(entity);
			sourceContainer->removeEntity(action.entity);

			std::cout << message << endl;

			//Moves obstacle entity
			moveObstacle(entity, adventure);
		}
		else
		{
			std::cout << "You cannot carry that item." << endl;
		}
	}
	else
	{
		std::cout << "That item isn't here." << endl;
	}
}

void Move::execute(Adventure *adventure, Action action)
{
	Player* playerC = (Player *)adventure->player->getComponent("PLAYER");
	Entity* currentRoom = adventure->roomContainer->getEntityByName(playerC->currentRoomId);
	Room* roomC = (Room *)currentRoom->getComponent("ROOM");
	Container* containerC;

	if (roomC->hasConnectionInDir(action.entity))
	{
		playerC->move(roomC->getConnection(action.entity));

		currentRoom = adventure->roomContainer->getEntityByName(playerC->currentRoomId);
		std::cout << "  --  " << currentRoom->name << "  --  " << endl;
		std::cout << currentRoom->desc << endl;

		containerC = (Container *)currentRoom->getComponent("CONTAINER");
		containerC->printContents("");
	}
}

void Pickup::execute(Adventure *adventure, Action action)
{
	Player* playerC = (Player *)adventure->player->getComponent("PLAYER");
	Entity* currentRoom = adventure->roomContainer->getEntityByName(playerC->currentRoomId);

	moveEntity(currentRoom, adventure->player, adventure, action, "Taken.");
}

void Drop::execute(Adventure *adventure, Action action)
{
	Player* playerC = (Player *)adventure->player->getComponent("PLAYER");
	Entity* currentRoom = adventure->roomContainer->getEntityByName(playerC->currentRoomId);

	moveEntity(adventure->player, currentRoom, adventure, action, "Dropped.");
}

//TakeFrom
//Take an item from within another item in the current room or player inventory and put that item in the player inventory.
void TakeFrom::execute(Adventure *adventure, Action action)
{
	Player* playerC = (Player *)adventure->player->getComponent("PLAYER");
	Entity* currentRoom = adventure->roomContainer->getEntityByName(playerC->currentRoomId);
	Container* roomContainer = (Container *)currentRoom->getComponent("CONTAINER");
	Container* playerContainer = (Container *)adventure->player->getComponent("CONTAINER");

	Entity* targetEntity;

	if (roomContainer->containsEntity(action.container))
	{
		targetEntity = roomContainer->getEntityByName(action.container);
	}
	else if (playerContainer->containsEntity(action.container))
	{
		targetEntity = playerContainer->getEntityByName(action.container);
	}
	else
	{
		std::cout << "There is no " << action.container << " here." << endl;
		return;
	}

	moveEntity(targetEntity, adventure->player, adventure, action, "Taken.");
}

//Put an item from the player inventory into a target container
void PutIn::execute(Adventure *adventure, Action action)
{
	Player* playerC = (Player *)adventure->player->getComponent("PLAYER");
	Entity* currentRoom = adventure->roomContainer->getEntityByName(playerC->currentRoomId);
	Container* roomContainer = (Container *)currentRoom->getComponent("CONTAINER");
	Container* playerContainer = (Container *)adventure->player->getComponent("CONTAINER");

	Entity* targetEntity;

	if (roomContainer->containsEntity(action.container))
	{
		targetEntity = roomContainer->getEntityByName(action.container);
	}
	else if (playerContainer->containsEntity(action.container))
	{
		targetEntity = playerContainer->getEntityByName(action.container);
	}
	else
	{
		std::cout << "There is no " << action.container << " here." << endl;
		return;
	}

	moveEntity(adventure->player, targetEntity, adventure, action, "Placed.");
}

void OpenWith::execute(Adventure *adventure, Action action)
{
	Player* playerC = (Player *)adventure->player->getComponent("PLAYER");
	Entity* currentRoom = adventure->roomContainer->getEntityByName(playerC->currentRoomId);
	Container* roomContainer = (Container *)currentRoom->getComponent("CONTAINER");
	Container* playerContainer = (Container *)adventure->player->getComponent("CONTAINER");

	Entity* targetEntity;

	//Check if the locked container is accessible
	if (roomContainer->containsEntity(action.container))
	{
		targetEntity = roomContainer->getEntityByName(action.container);
	}
	else if (playerContainer->containsEntity(action.container))
	{
		targetEntity = playerContainer->getEntityByName(action.container);
	}
	else
	{
		std::cout << "There is no " << action.container << " here." << endl;
		return;
	}

	//Check if the player has the key specified
	if (!playerContainer->containsEntity(action.entity))
	{
		std::cout << "You don't have a " << action.entity << "." << endl;
		return;
	}
	
	Message message = Message();
	message.senderId = action.entity;
	message.subscriberId = action.container;
	message.component = "LOCKABLE";
	message.info = "UNLOCK";

	action.mb->sendMessage(message);
}

//Inventory
//Prints the contents of the players inventory
void Inventory::execute(Adventure *adventure, Action action)
{
	Container* playerContainer = (Container *)adventure->player->getComponent("CONTAINER");
	playerContainer->printContents("Your inventory is empty.");
}

//LookIn
//Prints the contents of the target entity container
void LookIn::execute(Adventure *adventure, Action action)
{
	Player* playerC = (Player *)adventure->player->getComponent("PLAYER");
	Entity* currentRoom = adventure->roomContainer->getEntityByName(playerC->currentRoomId);
	Container* roomContainer = (Container *)currentRoom->getComponent("CONTAINER");
	Container* playerContainer = (Container *)adventure->player->getComponent("CONTAINER");

	Entity* targetEntity;

	if (roomContainer->containsEntity(action.container))
	{
		targetEntity = roomContainer->getEntityByName(action.container);
	}
	else if (playerContainer->containsEntity(action.container))
	{
		targetEntity = playerContainer->getEntityByName(action.container);
	}
	else
	{
		std::cout << "There is no " << action.container << " here." << endl;
		return;
	}

	if (targetEntity->hasComponent("LOCKABLE"))
	{
		Lockable *lockable = (Lockable *)targetEntity->getComponent("LOCKABLE");

		if (lockable->isLocked)
		{
			std::cout << "The " << targetEntity->name << " is locked. You will need its key." << endl;
			return;
		}
	}

	if (targetEntity->hasComponent("CONTAINER"))
	{
		Container* container = (Container *)targetEntity->getComponent("CONTAINER");
		container->printContents("The " + targetEntity->name + " is empty.");
	}
	else
	{
		std::cout << "You cannot look inside the " << targetEntity->name << "." << endl;
	}
}

CommandManager::CommandManager(Adventure *adventure, MessageBoard *mb)
{
	this->adventure = adventure;
	this->mb = mb;
	this->nouns = loadDictionary("data/nouns.txt");
	this->verbs = loadDictionary("data/verbs.txt");
}

std::map<std::string, std::string> CommandManager::loadDictionary(std::string path)
{
	std::map<std::string, std::string> dictionary;

	std::string line;
	std::string temp;
	std::string key;
	std::string value;
	bool isKey = true;

	ifstream myfile(path);
	
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			std::istringstream ss(line);
			while (std::getline(ss, temp, '='))
			{
				if (isKey)
				{
					key = temp;
				}
				else
				{
					value = temp;
					dictionary.insert(make_pair(value, key));
				}
				isKey = !isKey;
			}
		}
		myfile.close();
	}
	else
	{
		cout << "Unable to open Dictionary file";
	}

	return dictionary;
}

bool CommandManager::checkVerbs(std::string input)
{
	if (verbs.find(input) == verbs.end() || input == "MOVE")
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool CommandManager::checkNouns(std::string input)
{
	if (nouns.find(input) == nouns.end() || input == "MOVE")
	{
		return false;
	}
	else
	{
		return true;
	}
}

void CommandManager::recieveCommand(std::string input)
{
	if (input == "QUIT")
	{
		std::cout << "YOU HAVE DECIDED TO QUIT" << endl;
		lineBreak();
		return;
	}

	std::vector<std::string> commands = split(input, " ");

	bool isValidCommand = true;
	for (unsigned int i = 0; i < commands.size(); i++)
	{
		if (!checkVerbs(commands[i]) && !checkNouns(commands[i]))
		{
			isValidCommand = false;
			std::cout << "\"" << commands[i] << "\" is not a recognised action or entity!" << endl;
			break;
		}
		else if(checkNouns(commands[i]))
		{
			commands[i] = nouns[commands[i]];
		}
		else if (checkVerbs(commands[i]))
		{
			commands[i] = verbs[commands[i]];
		}
	}

	

	Action action = Action();
	action.mb = this->mb;

	if (isValidCommand)
	{
		if (commands.size() == 1)
		{
			if (commands[0] == "N" || commands[0] == "E" || commands[0] == "S" || commands[0] == "W" || commands[0] == "NE" || commands[0] == "NW" || commands[0] == "SE" || commands[0] == "SW")
			{
				Move* cmd = new Move();
				action.cmd = cmd;
				action.entity = commands[0];
			}
			else if (commands[0] == "INVENTORY")
			{
				Inventory* cmd = new Inventory();
				action.cmd = cmd;
			}
			else if (commands[0] == "QUIT")
			{
				return;
			}
			else
			{
				std::cout << "That command is invalid." << endl;
				isValidCommand = false;
			}
		}
		else if (commands.size() == 2)
		{
			if (commands[0] == "PICKUP")
			{
				action.cmd = new Pickup();
				action.entity = commands[1];
				action.container = "INVENTORY";
			}
			else if (commands[0] == "DROP")
			{
				action.cmd = new Drop();
				action.entity = commands[1];
			}
			else
			{
				std::cout << "That command is invalid." << endl;
				isValidCommand = false;
			}
		}
		else if (commands.size() == 3)
		{
			if (commands[0] == "LOOK" && commands[1] == "IN" && checkNouns(commands[2]))
			{
				action.cmd = new LookIn();
				action.container = commands[2];
			}
			else
			{
				std::cout << "That command is invalid." << endl;
				isValidCommand = false;
			}
		}
		else if (commands.size() == 4)
		{
			if (commands[0] == "PICKUP" && checkNouns(commands[1]) && commands[2] == "FROM" && checkNouns(commands[3]))
			{
				action.cmd = new TakeFrom();
				action.entity = commands[1];
				action.container = commands[3];
			}
			else if (commands[0] == "PUT" && checkNouns(commands[1]) && commands[2] == "IN" && checkNouns(commands[3]))
			{
				action.cmd = new PutIn();
				action.entity = commands[1];
				action.container = commands[3];
			}
			else if (commands[0] == "OPEN" && checkNouns(commands[1]) && commands[2] == "WITH" && checkNouns(commands[3]))
			{
				action.cmd = new OpenWith();
				action.entity = commands[3];
				action.container = commands[1];
			}
			else
			{
				std::cout << "That command is invalid." << endl;
				isValidCommand = false;
			}
		}
		if (isValidCommand)
		{
			action.cmd->execute(adventure, action);
			delete action.cmd;
		}
	}
}