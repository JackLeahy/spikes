#include <algorithm> 
#include <string>
#include <thread>
#include <future>
#include <sstream>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <map>

#include "Adventure.h"
#include "Commander.h"
#include "Utils.h"
#include "Entity.h"

using namespace std;

int runGame(std::string adventureName)
{
	MessageBoard *messageBoard = new MessageBoard();
	EntityFactory *entityFactory = new EntityFactory(messageBoard);
	Adventure *adventure = new Adventure(adventureName, entityFactory);
	CommandManager cm = CommandManager(adventure, messageBoard);

	std::string input;
	do
	{
		std::cout << ">";
		
		input = getLine();
		
		cm.recieveCommand(input);
	} while (input != "QUIT");
	
	delete adventure, entityFactory, messageBoard;
	return 0;
}