#include "Entity.h"

bool Entity::hasComponent(std::string name)
{
	if (components.find(name) == components.end())
	{
		return false;
	}
	else
	{
		return true;
	}
}

Component* Entity::getComponent(std::string name)
{
	Component* c = components[name];
	return c;
}

void Entity::addComponent(std::string name, Component* component)
{
	components.insert(make_pair(name, component));
}

void Entity::removeComponent(std::string name)
{
	delete getComponent(name);
	components.erase(name);
}

EntityFactory::EntityFactory(MessageBoard* mb)
{
	this->mb = mb;
}

EntityFactory::~EntityFactory()
{
	for (unsigned i = 0; i < entities.size(); i++)
	{
		delete entities[i];
	}
	for (unsigned i = 0; i < components.size(); i++)
	{
		delete components[i];
	}
}

Entity* EntityFactory::constructEntity(const Value& jsonEntity)
{
	Entity *entity = new Entity();

	entity->id = jsonEntity["id"].GetString();
	entity->name = jsonEntity["name"].GetString();
	entity->desc = jsonEntity["desc"].GetString();

	const Value& jsonComponents = jsonEntity["components"];

	for (SizeType i = 0; i < jsonComponents.Size(); i++)
	{
		std::string cId = jsonComponents[i]["id"].GetString();
		entity->addComponent(cId, constructComponent(jsonComponents[i], cId));
	}

	Messenger* messenger = new Messenger();
	components.push_back(messenger);

	//Add messageboard pointer for later access;
	messenger->mb = this->mb;
	messenger->self = entity;
	messenger->subscribe();
	entity->addComponent("MESSENGER", messenger);

	//memory management
	entities.push_back(entity);
	components.push_back(messenger);

	return entity;
}

Component* EntityFactory::constructComponent(const Value& jsonComponent, std::string cId)
{
	if (cId == "CONTAINER")
	{
		Container* component = new Container();

		const Value& jsonInventory = jsonComponent["inventory"];

		for (SizeType i = 0; i < jsonInventory.Size(); i++)
		{
			component->addEntity(this->constructEntity(jsonInventory[i]));
		}

		//memory management
		components.push_back(component);

		return component;
	}
	else if (cId == "CARRYABLE")
	{
		Carryable* component = new Carryable();

		component->encumberment = jsonComponent["encumberment"].GetInt();

		//memory management
		components.push_back(component);

		return component;
	}
	else if (cId == "LOCKABLE")
	{
		Lockable* component = new Lockable();

		component->isLocked = jsonComponent["locked"].GetBool();
		component->keyName = jsonComponent["key"].GetString();
		component->desc = jsonComponent["desc"].GetString();

		//memory management
		components.push_back(component);

		return component;
	}
	else if (cId == "ROOM")
	{
		Room* component = new Room();

		for (SizeType i = 0; i < jsonComponent["connections"].Size(); i++)
		{
			component->addConnection(jsonComponent["connections"][i]["dir"].GetString(), jsonComponent["connections"][i]["id"].GetString());
		}

		//memory management
		components.push_back(component);

		return component;
	}
	else if (cId == "OBSTACLE")
	{
		Obstacle* component = new Obstacle();

		component->dir = jsonComponent["dir"].GetString();
		component->connection = jsonComponent["connection"].GetString();

		//memory management
		components.push_back(component);

		return component;
	}
	else
	{
		std::cout << "--!!!--ERROR ENCOUNTERED IN COMPONENT-FACTORY--!!!--";
		return 0;
	}
}