#ifndef COMPONENT_H
#define COMPONENT_H

#include <algorithm> 
#include <string>
#include <thread>
#include <future>
#include <sstream>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <map>

#include "Message.h"
#include "Entity.h"

using namespace std;

class Entity;
class Message;

class Component
{
public:
	virtual ~Component() {}
};

class Carryable : public Component
{
public:
	int encumberment;
	std::string desc;
};

class Lockable : public Component
{
public:
	virtual void unlock(std::string keyName);
	virtual void lock();
	bool isLocked;
	std::string keyName;
	std::string desc;
};

class Container : public Component
{
public:
	virtual void addEntity(Entity *entity);
	virtual void removeEntity(std::string entityName);
	virtual void printContents(std::string message);
	virtual bool containsEntity(std::string entityName);
	virtual Entity* getEntityAtIndex(int index);
	virtual Entity* getEntityByName(std::string entityName);
private:
	std::vector<Entity*> inventory;
};

class Messenger : public Component
{
public:
	virtual void subscribe();
	virtual void receiveMessage(Message message);
	virtual void requestFromBlackboard();

	Entity* self;
	MessageBoard* mb;
};

class Player : public Component
{
public:
	virtual void move(std::string roomId);
	//virtual void changeScore(int increment);
	std::string	currentRoomId;
private:
	int score;
};

class Room : public Component
{
public:
	virtual bool hasConnectionInDir(std::string dir);
	virtual std::string getConnection(std::string dir);
	virtual void addConnection(std::string dir, std::string connection);
	virtual void removeConnection(std::string dir);
	std::map<std::string, std::string> connections;
};

class Obstacle : public Component
{
public:
	std::string dir;
	std::string connection;
};

#endif