#ifndef COMMANDER_H
#define COMMANDER_H

class Action;

class Command
{
public:
	virtual void execute(Adventure *adventure, Action action) = 0;
};

class Action
{
public:
	std::string entity;
	std::string container;
	MessageBoard *mb;
	Command *cmd;
};

class Move : public Command
{
public:
	void execute(Adventure *adventure, Action action);
};

class Pickup : public Command
{
	void execute(Adventure *adventure, Action action);
};

class Drop : public Command
{
	void execute(Adventure *adventure, Action action);
};

class Inventory : public Command
{
public:
	void execute(Adventure *adventure, Action action);
};

class LookIn : public Command
{
public:
	void execute(Adventure *adventure, Action action);
};

class TakeFrom : public Command
{
public:
	void execute(Adventure *adventure, Action action);
};

class PutIn : public Command
{
public:
	void execute(Adventure *adventure, Action action);
};

class OpenWith : public Command
{
public:
	void execute(Adventure *adventure, Action action);
};

class CommandManager
{
public:
	CommandManager(Adventure *adventure, MessageBoard *mb);
	std::map<std::string, std::string> loadDictionary(std::string path);
	void recieveCommand(std::string input);
	bool checkVerbs(std::string input);
	bool checkNouns(std::string input);
private:
	MessageBoard *mb;
	Adventure *adventure;
	std::map<std::string, std::string> nouns;
	std::map<std::string, std::string> verbs;
};

#endif
