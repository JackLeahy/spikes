#ifndef ADVENTURE_H 
#define ADVENTURE_H

#include "Entity.h"

using namespace std;

class Adventure
{
public:
	Adventure(std::string adventureName, EntityFactory* entityFactory);
	~Adventure();
	virtual void loadAdventure(std::string path);
	virtual void loadPlayer();

	EntityFactory* entityFactory;
	Entity* player;
	Container* roomContainer;

	std::string startRoom;
};

#endif