#ifndef ENTITY_H
#define ENTITY_H

#include <algorithm> 
#include <string>
#include <thread>
#include <future>
#include <sstream>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <map>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "Component.h"
#include "Message.h"

using namespace rapidjson;

class Component;
class MessageBoard;

class Entity
{
public:
	virtual bool hasComponent(std::string name);
	virtual Component* getComponent(std::string name);
	virtual void addComponent(std::string name, Component* component);
	virtual void removeComponent(std::string name);

	std::string id;
	std::string name;
	std::string desc;
	std::string origin;
protected:
	std::map<std::string, Component*> components;
};

class EntityFactory
{
public:
	EntityFactory(MessageBoard* mb);
	~EntityFactory();
	virtual Entity* constructEntity(const Value& jsonEntity);
	virtual Component* constructComponent(const Value& jsonEntity, std::string cId);

	MessageBoard *mb;
private:
	std::vector<Entity*> entities;
	std::vector<Component*> components;
};

#endif