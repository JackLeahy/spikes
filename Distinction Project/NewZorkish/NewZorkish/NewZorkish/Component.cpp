#include "Component.h"

//---------------------------------
//--CONTAINER COMPONENT
//---------------------------------

void Container::addEntity(Entity *entity)
{
	inventory.push_back(entity);
}

void Container::removeEntity(std::string entityName)
{
	for (unsigned i = 0; i < this->inventory.size(); i++)
	{
		if (inventory[i]->id == entityName)
		{
			this->inventory.erase(this->inventory.begin() + i);
			break;
		}
	}
}

void Container::printContents(std::string message)
{
	if (this->inventory.size() > 0)
	{
		if (message == "Your inventory is empty.")
		{
			std::cout << "   --|INVENTORY|--  " << endl;
		}
		for (unsigned i = 0; i < this->inventory.size(); i++)
		{
			if (message == "Your inventory is empty.")
			{
				std::cout << getEntityAtIndex(i)->name << endl;
			}
			else
			{
				std::cout << getEntityAtIndex(i)->desc << endl;
			}
			
		}
	}
	else
	{
		if (message != "")
		{
			std::cout << message << endl;
		}
	}
}

Entity* Container::getEntityAtIndex(int index)
{
	return inventory[index];
}

Entity* Container::getEntityByName(std::string entityName)
{
	for (unsigned i = 0; i < this->inventory.size(); i++)
	{
		if (inventory[i]->id == entityName)
		{
			return inventory[i];
		}
	}
}

bool Container::containsEntity(std::string entityName)
{
	for (unsigned i = 0; i < this->inventory.size(); i++)
	{
		if (inventory[i]->id == entityName)
		{
			return true;
		}
	}
	return false;
}

//---------------------------------
//--LOCKABLE COMPONENT
//---------------------------------

void Lockable::unlock(std::string keyName)
{
	if (keyName == this->keyName)
	{
		std::cout << this->desc << endl;
		this->isLocked = false;
	}
	else
	{
		std::cout << "That failed to unlock it.";
	}
}

void Lockable::lock()
{
	this->isLocked = true;
}

//---------------------------------
//--ROOM COMPONENT
//---------------------------------

bool Room::hasConnectionInDir(std::string dir)
{
	if (this->connections.count(dir) != 1)
	{
		std::cout << "You cannot go in that direction." << endl;
		return false;
	}
	return true;
}

std::string Room::getConnection(std::string dir)
{
	return this->connections[dir];
}

void Room::addConnection(std::string dir, std::string connection)
{
	connections.insert(make_pair(dir, connection));
}

void Room::removeConnection(std::string dir)
{
	connections.erase(dir);
}

//---------------------------------
//--PLAYER COMPONENT
//---------------------------------

void Player::move(std::string roomId)
{
	currentRoomId = roomId;
	//changeScore(1);
}

//---------------------------------
//--MESSENGER COMPONENT
//---------------------------------

void Messenger::subscribe()
{
	this->mb->subscribe(self->id, this);
}

void Messenger::receiveMessage(Message message)
{
	if (message.component == "LOCKABLE")
	{
		if (this->self->hasComponent("LOCKABLE"))
		{
			Lockable* lockable = (Lockable *)this->self->getComponent("LOCKABLE");
			if (lockable->isLocked)
			{
				lockable->unlock(message.senderId);
			}
			else
			{
				std::cout << "This item is already unlocked." << endl;
			}
		}
		else
		{
			std::cout << "This item cannot be locked or unlocked." << endl;
		}
	}
	else
	{
		cout << "ERROR" << endl;
	}
}

void Messenger::requestFromBlackboard()
{
	std::vector<Message> messages = mb->requestFromBlackboard(this->self->id);

	for (unsigned i = 0; i < messages.size(); i++)
	{
		this->receiveMessage(messages[i]);
	}
}