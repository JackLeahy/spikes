#ifndef MESSAGE_H
#define MESSAGE_H

#include <algorithm>
#include <string>
#include <future>
#include <sstream>
#include <iostream>
#include <fstream>
#include <map>

#include "Component.h"

using namespace std;

class Messenger;

class Message
{
public:
	std::string subscriberId;
	std::string senderId;
	std::string component;
	std::string info;
	bool deleteAfterRead = false;
};

class Subscriber
{
public:
	std::string subscriberId;
	Messenger* reciever;
};

class MessageBoard
{
public:
	virtual void subscribe(std::string id, Messenger* messenger);
	virtual void sendMessage(Message message);

	virtual std::vector<Message> requestFromBlackboard(std::string id);
	virtual void postToBlackboard(Message message);

	std::vector<Subscriber> subscribers;
	std::vector<Message> blackboard;
};

#endif