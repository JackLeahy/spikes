#include <sstream>
#include <iostream>
#include <fstream>
#include <algorithm> 
#include <string>
#include <future>

using namespace std;

void lineBreak()
{
	std::cout << "----------------------------------------------------------" << endl;
}

std::string getLine()
{
	std::string input;

	getline(cin, input);
	std::transform(input.begin(), input.end(), input.begin(), ::toupper);

	return input;
}

std::vector<std::string> split(std::string str, std::string del)
{
	std::vector<std::string> result;
	std::istringstream iss(str);
	for (std::string s; iss >> s; )
		result.push_back(s);

	return result;
}