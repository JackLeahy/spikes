#ifndef MESSAGE_H
#define MESSAGE_H

#include <algorithm> 
#include <string>
#include <future>
#include <sstream>
#include <iostream>
#include <fstream>
#include <map>

#include "Item.h"

using namespace std;

class Message
{
public:
	std::string subscriberId;
	std::string senderId;
	std::string component;
	std::string info;
	bool deleteAfterRead = false;
};

class Subscriber
{
public:
	std::string subscriberId;
	Item* reciever;
};

class MessageBoard
{
public:
	virtual void subscribe(std::string id, Item* item);
	virtual void sendMessage(Message message);

	virtual std::vector<Message> requestFromBlackboard(std::string id);
	virtual void postToBlackboard(Message message);

	std::vector<Subscriber> subscribers;
	std::vector<Message> blackboard;
};

#endif