#ifndef ITEM_H
#define ITEM_H

#include <algorithm> 
#include <string>
#include <thread>
#include <future>
#include <sstream>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <map>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "Component.h"
#include "Message.h"

using namespace rapidjson;

class Component;
class MessageBoard;
class Message;

class Item
{
public:
	virtual void loadItem(const Value& jsonItem, MessageBoard &mb);
	//virtual int indexByName(std::string itemName);
	//virtual void removeItem(int itemIndex);

	//Register this to a message subscribe list
	virtual void subscribe(MessageBoard &mb);
	//Execute actions depending on the message
	virtual void receiveMessage(Message message);
	//Look a message in a blackboard
	virtual void requestFromBlackboard();

	virtual bool hasComponent(std::string name);
	virtual Component* getComponent(std::string name);
	virtual void setComponent(std::string name, Component* component);

	std::string id;
	std::string name;
	std::string desc;
	std::string origin;

	MessageBoard *mb;
	//std::vector<Item> contains;
protected:
	std::map<std::string, Component*> components;
};

#endif