#include <algorithm> 
#include <string>
#include <future>
#include <sstream>
#include <iostream>
#include <fstream>
#include <map>

#include "Adventure.h"
#include "Commander.h"

using namespace std;

std::map<std::string, std::string> loadDictionary()
{
	std::map<std::string, std::string> dictionary;

	ifstream myfile("data/dictionary.txt");

	std::string line;
	std::string temp;
	std::string key;
	std::string value;
	bool isKey = true;

	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			std::istringstream ss(line);
			while (std::getline(ss, temp, '='))
			{
				if (isKey)
				{
					key = temp;
				}
				else
				{
					value = temp;
					dictionary.insert(make_pair(value, key));
				}
				isKey = !isKey;
			}
		}
		myfile.close();
	}
	else
	{
		cout << "Unable to open Dictionary file";
	}

	return dictionary;
}

Action::Action()
{

}

void Move::execute(Adventure *adventure, Action action)
{
	adventure->movePlayerInDir(action.target);
}

void Pickup::execute(Adventure *adventure, Action action)
{
	adventure->pickupItem(action.entity);
}


void Drop::execute(Adventure *adventure, Action action)
{
	adventure->dropItem(action.entity);
}

void Inventory::execute(Adventure *adventure, Action action)
{
	adventure->printInventory();
}

CommandManager::CommandManager(Adventure *adventure)
{
	this->adventure = adventure;
	this->dictionary = loadDictionary();
}

bool CommandManager::checkDictionary(std::string input)
{
	if (dictionary.find(input) == dictionary.end())
	{
		std::cout << "\"" << input << "\" is not a recognised command!" << endl;
		return false;
	}
	else
	{
		return true;
	}
}

void CommandManager::recieveCommand(std::string input)
{
	Action action = Action();
	
	if (checkDictionary(input))
	{
		input = dictionary[input];
		action.target = input;
		
		if (input == "N" || input == "E" || input == "S" || input == "W" || input == "NE" || input == "NW" || input == "SE" || input == "SW")
		{
			Move move;
			action.target = input;
			move.execute(adventure, action);
		}
		else if (input == "INVENTORY")
		{
			Inventory inventory;
			inventory.execute(adventure, action);
		}
		else
		{
			// do nothing;
		}
	}
}