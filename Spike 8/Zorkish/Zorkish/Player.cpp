#include <algorithm> 
#include <string>
#include <thread>
#include <future>
#include <sstream>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <map>

#include "Player.h"

Player::Player()
{

}

void Player::changeScore(int increment)
{
	score += increment;
}

void Player::move(int roomId)
{
	currentRoomId = roomId;
	changeScore(1);
}

void Player::pickupItem(Item item)
{
	inventory.push_back(item);
}

void Player::dropItem(std::string itemName)
{
	int itemIndex = indexByName(itemName);
	inventory.erase(inventory.begin() + itemIndex);
}

int Player::indexByName(std::string itemName)
{
	for (unsigned i = 0; i < inventory.size(); i++)
	{
		if (inventory[i].name == itemName)
		{
			return i;
		}
	}
	return -1;
}