// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "CustomVector.h"


// Sets default values for this component's properties
UCustomVector::UCustomVector()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UCustomVector::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UCustomVector::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	// ...
}

void UCustomVector::SetX(float newX)
{
	UCustomVector::x = newX;
}

float UCustomVector::GetX()
{
	return UCustomVector::x;
}

void UCustomVector::SetY(float newY)
{
	UCustomVector::y = newY;
}

float UCustomVector::GetY()
{
	return UCustomVector::y;
}

void UCustomVector::SetZ(float newZ)
{
	UCustomVector::z = newZ;
}

float UCustomVector::GetZ()
{
	return UCustomVector::z;
}