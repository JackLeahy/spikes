// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "CustomVector.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MYPROJECT_API UCustomVector : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCustomVector();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "X-Value", CompactNodeTitle = "X-Value", Keywords = "x"), Category = Vector)
	float x;
	UPROPERTY(EditAnywhere, meta = (DisplayName = "Y-Value", CompactNodeTitle = "Y-Value", Keywords = "y"), Category = Vector)
	float y;
	UPROPERTY(EditAnywhere, meta = (DisplayName = "Z-Value", CompactNodeTitle = "Z-Value", Keywords = "z"), Category = Vector)
	float z;

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Set X", CompactNodeTitle = "Set X", Keywords = "Set X"), Category = Vector)
	void SetX(float newX);

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Get X", CompactNodeTitle = "Get X", Keywords = "Get X"), Category = Vector)
	float GetX();

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Set Y", CompactNodeTitle = "Set Y", Keywords = "Set Y"), Category = Vector)
	void SetY(float newY);

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Get Y", CompactNodeTitle = "Get Y", Keywords = "Get Y"), Category = Vector)
	float GetY();

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Set Z", CompactNodeTitle = "Set Z", Keywords = "Set Z"), Category = Vector)
	void SetZ(float newZ);

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Get Z", CompactNodeTitle = "Get Z", Keywords = "Get Z"), Category = Vector)
	float GetZ();
};
