#ifndef ADVENTURE_H 
#define ADVENTURE_H

#include "Dungeon.h"
#include "Player.h"

using namespace std;

class Adventure
{
public:
	virtual void createAdventure(std::string adventure);
	virtual void movePlayerInDir(std::string dir);
	virtual void movePlayerToRoom(int roomId);
	Dungeon dungeon;
	Player player;
};

#endif