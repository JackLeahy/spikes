#ifndef INVENTORY_H
#define INVENTORY_H

#include <algorithm> 
#include <string>
#include <thread>
#include <future>
#include <sstream>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <map>

#include "Item.h"

using namespace std;

class Inventory
{
public:
	void printContents()
	{
		std::cout << "|#|    ITEM NAME    " << endl;
		std::cout << "|-|-----------------" << endl;
		for (unsigned i = 0; i < inventory.size(); i++)
		{
			std::cout << "|" << i << "|    " << inventory[i].name << endl;
		}
		std::cout << "|-|-----------------" << endl << endl;
	}

	int indexByName(std::string itemName)
	{
		for (unsigned i = 0; i < inventory.size(); i++)
		{
			if (inventory[i].name == itemName)
			{
				return i;
			}
		}
		return -1;
	}

	void addItem(Item item)
	{
		inventory.push_back(item);
	}

	void removeItem(std::string itemName)
	{
		int itemIndex = indexByName(itemName);
		inventory.erase(inventory.begin() + itemIndex);
	}
private:
	std::vector<Item> inventory;
};

#endif