#ifndef ITEM_H
#define ITEM_H

#include <algorithm> 
#include <string>
#include <thread>
#include <future>
#include <sstream>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <map>

class Item
{
public:
	std::string name;
	std::string desc;
};

#endif