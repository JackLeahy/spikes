#ifndef DUNGEON_H
#define DUNGEON_H

#include "Room.h"

class Dungeon
{
public:
	virtual void buildDungeon(std::string path);
	virtual bool hasConnectionInDir(std::string dir, int roomId);
	virtual int getRoomConnection(std::string dir, int currentRoomId);

	int startRoom;
	std::vector<Room> rooms;
};

#endif

