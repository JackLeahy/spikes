#ifndef COMMANDER_H
#define COMMANDER_H

class Command
{
public:
	virtual void execute(std::string dir) = 0;
	Adventure *adventure;
};

class Move : public Command
{
public:
	Move(Adventure *adventure);
	void execute(std::string dir);
};

class CommandManager
{
public:
	CommandManager(Adventure *adventure);
	void recieveCommand(std::string input);
	bool checkDictionary(std::string input);
private:
	Adventure *adventure;
	std::map<std::string, std::string> dictionary;
};

#endif
