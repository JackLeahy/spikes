#ifndef GAMEENGINE_H
#define GAMEENGINE_H

#include <string>

using namespace std;

int runGame(std::string adventureName);

#endif