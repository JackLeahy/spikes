#include <algorithm> 
#include <string>
#include <map>

#include "rapidjson/document.h"

#include "Room.h"

using namespace std;
using namespace rapidjson;

Room::Room(const Value& jsonRoom)
{
	id = jsonRoom["id"].GetInt();
	name = jsonRoom["name"].GetString();
	sDesc = jsonRoom["sDesc"].GetString();
	//lDesc = jsonroom["lDesc"].GetString();

	for (SizeType i = 0; i < jsonRoom["connections"].Size(); i++)
	{
		connections.insert(make_pair(jsonRoom["connections"][i]["dir"].GetString(), jsonRoom["connections"][i]["id"].GetInt()));
	}
}