#include <algorithm> 
#include <string>
#include <thread>
#include <future>
#include <sstream>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <map>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "Room.h"
#include "Dungeon.h"

using namespace std;
using namespace rapidjson;

void Dungeon::buildDungeon(std::string path)
{
	std::ifstream in(path);
	std::string contents((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());

	const char* json = contents.c_str();

	Document document;
	document.Parse(json);

	const Value& jsonRooms = document["rooms"];

	for (SizeType i = 0; i < jsonRooms.Size(); i++)
	{
		Room room = Room(jsonRooms[i]);
		rooms.push_back(room);
	}
	const Value& jsonStart = document["startRoom"];
	startRoom = jsonStart.GetInt();
}

bool Dungeon::hasConnectionInDir(std::string dir, int roomId)
{
	if (rooms[roomId].connections.find(dir) == rooms[roomId].connections.end())
	{
		std::cout << "You cannot go in that direction." << endl;
		return false;
	}
	return true;
}

int Dungeon::getRoomConnection(std::string dir, int currentRoomId)
{
	return rooms[currentRoomId].connections[dir];
}