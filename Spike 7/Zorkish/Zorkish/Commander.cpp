#include <algorithm> 
#include <string>
#include <future>
#include <sstream>
#include <iostream>
#include <fstream>
#include <map>

#include "Adventure.h"
#include "Commander.h"

using namespace std;

std::map<std::string, std::string> loadDictionary()
{
	std::map<std::string, std::string> dictionary;

	ifstream myfile("data/dictionary.txt");

	std::string line;
	std::string temp;
	std::string key;
	std::string value;
	bool isKey = true;

	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			std::istringstream ss(line);
			while (std::getline(ss, temp, '='))
			{
				if (isKey)
				{
					key = temp;
				}
				else
				{
					value = temp;
					dictionary.insert(make_pair(value, key));
				}
				isKey = !isKey;
			}
		}
		myfile.close();
	}
	else
	{
		cout << "Unable to open Dictionary file";
	}

	return dictionary;
}

Move::Move(Adventure *adventure)
{
	this->adventure = adventure;
}

void Move::execute(std::string dir)
{
	adventure->movePlayerInDir(dir);
}

CommandManager::CommandManager(Adventure *adventure)
{
	this->adventure = adventure;
	this->dictionary = loadDictionary();
}

bool CommandManager::checkDictionary(std::string input)
{
	if (dictionary.find(input) == dictionary.end())
	{
		std::cout << "\"" << input << "\" is not a recognised command!" << endl;
		return false;
	}
	else
	{
		return true;
	}
}

void CommandManager::recieveCommand(std::string input)
{
	if (checkDictionary(input))
	{
		input = dictionary[input];
		if (input == "N" || input == "E" || input == "S" || input == "W" || input == "NE" || input == "NW" || input == "SE" || input == "SW")
		{
			Move move = Move(adventure);
			move.execute(input);
		}
		else
		{
			// do nothing;
		}
	}
}