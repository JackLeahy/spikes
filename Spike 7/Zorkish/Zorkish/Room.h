#ifndef ROOM_H
#define ROOM_H

#include <algorithm> 
#include <string>
#include <future>
#include <sstream>
#include <iostream>
#include <fstream>
#include <map>

#include "rapidjson/document.h"

using namespace std;
using namespace rapidjson;

class Room
{
public:
	Room(const Value& jsonRoom);

	void printShortDesc()
	{
		std::cout << name << endl << sDesc << endl;
	}
	int id;
	std::string name;
	std::string sDesc;
	std::string lDesc;
	std::map<std::string, int> connections;
	//std::vector<int> items;
};

#endif
