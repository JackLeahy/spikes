#include <algorithm> 
#include <string>
#include <thread>
#include <future>
#include <sstream>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <map>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "Adventure.h"
#include "Dungeon.h"

using namespace std;
using namespace rapidjson;

const char* parseFile(std::string path)
{
	std::ifstream in(path);
	std::string contents((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());

	const char* json = contents.c_str();
	cout << json;

	return json;
}

void Adventure::createAdventure(std::string adventure)
{
	std::string path;
	if (adventure == "Adventure 1")
	{
		path = "data/testadventuremap.json";
	}
	else if (adventure == "Adventure 2")
	{

	}
	dungeon.buildDungeon(path);
	movePlayerToRoom(dungeon.startRoom);
}

void Adventure::movePlayerInDir(std::string dir)
{
	int roomId = dungeon.getRoomConnection(dir, player.currentRoomId);
	if (dungeon.hasConnectionInDir(dir, roomId))
	{
		movePlayerToRoom(roomId);
	}
}

void Adventure::movePlayerToRoom(int roomId)
{
	
	player.currentRoomId = roomId;
	std::cout << "  --  " << dungeon.rooms[player.currentRoomId].name << "  --  " << endl;
	std::cout << dungeon.rooms[player.currentRoomId].sDesc << endl;
}