#include <algorithm> 
#include <string>
#include <map>

#include "rapidjson/document.h"

#include "Room.h"

using namespace std;
using namespace rapidjson;

Room::Room(const Value& jsonRoom)
{
	id = jsonRoom["id"].GetInt();
	name = jsonRoom["name"].GetString();
	desc = jsonRoom["desc"].GetString();

	for (SizeType i = 0; i < jsonRoom["connections"].Size(); i++)
	{
		connections.insert(make_pair(jsonRoom["connections"][i]["dir"].GetString(), jsonRoom["connections"][i]["id"].GetInt()));
	}

	for (SizeType i = 0; i < jsonRoom["items"].Size(); i++)
	{
		Item item;
		item.id = jsonRoom["items"][i]["id"].GetString();
		item.name = jsonRoom["items"][i]["name"].GetString();
		item.desc = jsonRoom["items"][i]["desc"].GetString();
		item.origin = jsonRoom["items"][i]["origin"].GetString();

		for (SizeType j = 0; j < jsonRoom["items"][i]["contains"].Size(); j++)
		{
			Item cItem;
			cItem.id = jsonRoom["items"][i]["contains"][j]["id"].GetString();
			cItem.name = jsonRoom["items"][i]["contains"][j]["name"].GetString();
			cItem.desc = jsonRoom["items"][i]["contains"][j]["desc"].GetString();
			cItem.origin = jsonRoom["items"][i]["contains"][j]["origin"].GetString();
			item.contains.push_back(cItem);
		}

		items.push_back(item);
	}
}

bool Room::hasConnectionInDir(std::string dir)
{
	if (this->connections.count(dir) != 1)
	{
		std::cout << "You cannot go in that direction." << endl;
		return false;
	}
	return true;
}

int Room::getRoomConnection(std::string dir)
{
	return this->connections[dir];
}

int Room::indexByName(std::string itemName)
{
	for (unsigned i = 0; i < items.size(); i++)
	{
		if (items[i].id == itemName)
		{
			return i;
		}
	}
	return -1;
}