#include <algorithm> 
#include <string>
#include <thread>
#include <future>
#include <sstream>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <map>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "Room.h"
#include "Adventure.h"

using namespace std;
using namespace rapidjson;

const char* parseFile(std::string path)
{
	std::ifstream in(path);
	std::string contents((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());

	const char* json = contents.c_str();
	cout << json;

	return json;
}

Adventure::Adventure(std::string adventureName)
{
	std::string path;
	if (adventureName == "Adventure 1")
	{
		path = "data/ForestCaveAdventure.json";
	}
	else if (adventureName == "Adventure 2")
	{
		path = "data/testadventuremap.json";
	}
	loadAdventure(path);
	movePlayerToRoom(this->startRoom);
}

void Adventure::loadAdventure(std::string path)
{
	std::ifstream in(path);
	std::string contents((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());

	const char* json = contents.c_str();

	Document document;
	document.Parse(json);

	const Value& jsonRooms = document["rooms"];

	for (SizeType i = 0; i < jsonRooms.Size(); i++)
	{
		Room room = Room(jsonRooms[i]);
		rooms.push_back(room);
	}

	const Value& jsonStart = document["startRoom"];
	startRoom = jsonStart.GetInt();
}

void Adventure::pickupItem(std::string itemName)
{
	int itemIndex = rooms[player.currentRoomId].indexByName(itemName);

	if(itemIndex != -1)
	
	{
		player.pickupItem(rooms[player.currentRoomId].items[itemIndex]);
		rooms[player.currentRoomId].items.erase(rooms[player.currentRoomId].items.begin() + itemIndex);
		std::cout << "Taken." << endl;
	}
	else
	{
		std::cout << "That item isn't here." << endl;
	}
}

void Adventure::dropItem(std::string itemName)
{
	int itemIndex = player.indexByName(itemName);
	if (itemIndex != -1)
	{
		rooms[player.currentRoomId].items.push_back(player.inventory[itemIndex]);
		player.dropItem(itemIndex);
		std::cout << "You drop the " << itemName << "." << endl;
	}
	else
	{
		std::cout << "You aren't holding that item." << endl;
	}
}

void Adventure::lookInItem(std::string itemName)
{
	int roomIndex = rooms[player.currentRoomId].indexByName(itemName);
	int inventoryIndex = player.indexByName(itemName);
	if (roomIndex != -1)
	{
		if (rooms[player.currentRoomId].items[roomIndex].contains.size() > 0)
		{
			for (unsigned i = 0; i < rooms[player.currentRoomId].items[roomIndex].contains.size(); i++)
			{
				std::cout << rooms[player.currentRoomId].items[roomIndex].contains[i].origin << endl;
			}
		}
		else
		{
			std::cout << "There is nothing inside the " << itemName << "." << endl;
		}
	}
	else if (inventoryIndex != -1)
	{
		if (player.inventory[inventoryIndex].contains.size() > 0)
		{
			for (unsigned i = 0; i < player.inventory[inventoryIndex].contains.size(); i++)
			{
				std::cout << player.inventory[inventoryIndex].contains[i].desc << endl;
			}
		}
		else
		{
			std::cout << "There is nothing inside your " << itemName << "." << endl;
		}
	}
	else
	{
		std::cout << "There is no " << itemName << " here." << endl;
	}
}

void Adventure::takeFromContainer(std::string itemName, std::string containerName)
{
	int containerIndex;
	int itemIndex;
	Item* container;
	Item item;
	
	if (rooms[player.currentRoomId].indexByName(containerName) != -1)
	{
		containerIndex = rooms[player.currentRoomId].indexByName(containerName);
		container = &rooms[player.currentRoomId].items[containerIndex];
		itemIndex = container->indexByName(itemName);
		if (itemIndex != -1)
		{
			item = rooms[player.currentRoomId].items[containerIndex].contains[itemIndex];
		}
		else
		{
			std::cout << "There isn't a " << itemName << " inside the " << containerName << endl;
			return;
		}
	}
	else if(player.indexByName(containerName) != -1)
	{
		containerIndex = player.indexByName(containerName);
		container = &player.inventory[containerIndex];
		itemIndex = container->indexByName(itemName);
		if (itemIndex != -1)
		{
			item = player.inventory[containerIndex].contains[itemIndex];
		}
		else
		{
			std::cout << "There is nothing inside your " << itemName << "." << endl;
			return;
		}
	}
	else
	{
		std::cout << "There isn't that item here." << endl;
		return;
	}

	container->removeItem(itemIndex);
	player.pickupItem(item);
	std::cout << "Taken." << endl;

	delete container;
}

void Adventure::putInContainer(std::string itemName, std::string containerName)
{
	int containerIndex;
	int	itemIndex = player.indexByName(itemName);
	Item* container;
	Item* item = &player.inventory[itemIndex];

	if (itemIndex != -1)
	{
		if (rooms[player.currentRoomId].indexByName(containerName) != -1)
		{
			containerIndex = rooms[player.currentRoomId].indexByName(containerName);
			container = &rooms[player.currentRoomId].items[containerIndex];
			
		}
		else if(player.indexByName(containerName) != -1)
		{
			containerIndex = player.indexByName(containerName);
			container = &player.inventory[containerIndex];
		}
		else
		{
			std::cout << "There isn't that item here." << endl;
			return;
		}

		container->contains.push_back(*item);
		player.dropItem(itemIndex);
		std::cout << "You placed the " << itemName << " inside the " << containerName << "." << endl;
	}
	else
	{
		std::cout << "You don't have a " << itemName << "." << endl;
		return;
	}

	delete container, item;
}

void Adventure::printInventory()
{
	if (player.inventory.size() > 0)
	{
		std::cout << "   --|INVENTORY|--  " << endl;
		for (unsigned i = 0; i < player.inventory.size(); i++)
		{
			std::cout << player.inventory[i].name << endl;
		}
	}
	else
	{
		std::cout << "Your inventory is empty." << endl;
	}
}

void Adventure::movePlayerInDir(std::string dir)
{
	if (rooms[player.currentRoomId].hasConnectionInDir(dir))
	{
		movePlayerToRoom(rooms[player.currentRoomId].getRoomConnection(dir));
	}
}

void Adventure::movePlayerToRoom(int roomId)
{
	player.move(roomId);
	rooms[player.currentRoomId].printDesc();
}