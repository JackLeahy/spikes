#include <algorithm> 
#include <string>
#include <future>
#include <sstream>
#include <iostream>
#include <fstream>
#include <map>

#include "Adventure.h"
#include "Commander.h"
#include "Utils.h"

using namespace std;

void Move::execute(Adventure *adventure, Action action)
{
	adventure->movePlayerInDir(action.entity);
}

void Pickup::execute(Adventure *adventure, Action action)
{
	adventure->pickupItem(action.entity);
}

void Drop::execute(Adventure *adventure, Action action)
{
	adventure->dropItem(action.entity);
}

void Inventory::execute(Adventure *adventure, Action action)
{
	adventure->printInventory();
}

void LookIn::execute(Adventure *adventure, Action action)
{
	adventure->lookInItem(action.container);
}

void TakeFrom::execute(Adventure *adventure, Action action)
{
	adventure->takeFromContainer(action.entity, action.container);
}

void PutIn::execute(Adventure *adventure, Action action)
{
	adventure->putInContainer(action.entity, action.container);
}

CommandManager::CommandManager(Adventure *adventure)
{
	this->adventure = adventure;
	this->nouns = loadDictionary("data/nouns.txt");
	this->verbs = loadDictionary("data/verbs.txt");
}

std::map<std::string, std::string> CommandManager::loadDictionary(std::string path)
{
	std::map<std::string, std::string> dictionary;

	std::string line;
	std::string temp;
	std::string key;
	std::string value;
	bool isKey = true;

	ifstream myfile(path);
	
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			std::istringstream ss(line);
			while (std::getline(ss, temp, '='))
			{
				if (isKey)
				{
					key = temp;
				}
				else
				{
					value = temp;
					dictionary.insert(make_pair(value, key));
				}
				isKey = !isKey;
			}
		}
		myfile.close();
	}
	else
	{
		cout << "Unable to open Dictionary file";
	}

	return dictionary;
}

bool CommandManager::checkVerbs(std::string input)
{
	if (verbs.find(input) == verbs.end() || input == "MOVE")
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool CommandManager::checkNouns(std::string input)
{
	if (nouns.find(input) == nouns.end() || input == "MOVE")
	{
		return false;
	}
	else
	{
		return true;
	}
}

void CommandManager::recieveCommand(std::string input)
{
	std::vector<std::string> commands = split(input, " ");

	bool isValidCommand = true;
	for (unsigned int i = 0; i < commands.size(); i++)
	{
		if (!checkVerbs(commands[i]) && !checkNouns(commands[i]))
		{
			isValidCommand = false;
			std::cout << "\"" << commands[i] << "\" is not a recognised action or entity!" << endl;
			break;
		}
		else if(checkNouns(commands[i]))
		{
			commands[i] = nouns[commands[i]];
		}
		else if (checkVerbs(commands[i]))
		{
			commands[i] = verbs[commands[i]];
		}
	}

	Action action = Action();

	if (isValidCommand)
	{
		if (commands.size() == 1)
		{
			if (commands[0] == "N" || commands[0] == "E" || commands[0] == "S" || commands[0] == "W" || commands[0] == "NE" || commands[0] == "NW" || commands[0] == "SE" || commands[0] == "SW")
			{
				Move* cmd = new Move();
				action.cmd = cmd;
				action.entity = commands[0];
			}
			else if (commands[0] == "INVENTORY")
			{
				Inventory* cmd = new Inventory();
				action.cmd = cmd;
			}
			else
			{
				std::cout << "That command is invalid." << endl;
				isValidCommand = false;
			}
		}
		else if (commands.size() == 2)
		{
			if (commands[0] == "PICKUP")
			{
				action.cmd = new Pickup();
				action.entity = commands[1];
			}
			else if (commands[0] == "DROP")
			{
				action.cmd = new Drop();
				action.entity = commands[1];
			}
			else
			{
				std::cout << "That command is invalid." << endl;
				isValidCommand = false;
			}
		}
		else if (commands.size() == 3)
		{
			if (commands[0] == "LOOK" && commands[1] == "IN" && checkNouns(commands[2]))
			{
				action.cmd = new LookIn();
				action.container = commands[2];
			}
			else
			{
				std::cout << "That command is invalid." << endl;
				isValidCommand = false;
			}
		}
		else if (commands.size() == 4)
		{
			if (commands[0] == "PICKUP" && checkNouns(commands[1]) && commands[2] == "FROM" && checkNouns(commands[3]))
			{
				action.cmd = new TakeFrom();
				action.entity = commands[1];
				action.container = commands[3];
			}
			else if (commands[0] == "PUT" && checkNouns(commands[1]) && commands[2] == "IN" && checkNouns(commands[3]))
			{
				action.cmd = new PutIn();
				action.entity = commands[1];
				action.container = commands[3];
			}
			else
			{
				std::cout << "That command is invalid." << endl;
				isValidCommand = false;
			}
		}
		if (isValidCommand)
		{
			action.cmd->execute(adventure, action);
		}
	}
}