#ifndef ITEM_H
#define ITEM_H

#include <algorithm> 
#include <string>
#include <thread>
#include <future>
#include <sstream>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <map>

class Item
{
public:
	int indexByName(std::string itemName)
	{
		for (unsigned i = 0; i < contains.size(); i++)
		{
			if (contains[i].id == itemName)
			{
				return i;
			}
		}
		return -1;
	}

	void removeItem(int itemIndex)
	{
		contains.erase(contains.begin() + itemIndex);
	}
	std::string id;
	std::string name;
	std::string desc;
	std::string origin;
	std::vector<Item> contains;
};

#endif