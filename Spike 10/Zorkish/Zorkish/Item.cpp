#include "Item.h"

void Item::loadItem(const Value& jsonItem)
{
	this->id = jsonItem["id"].GetString();
	this->name = jsonItem["name"].GetString();
	this->desc = jsonItem["desc"].GetString();
	this->origin = jsonItem["origin"].GetString();

	const Value& jsonComponents = jsonItem["components"];

	for (SizeType i = 0; i < jsonComponents.Size(); i++)
	{
		std::string cId = jsonComponents[i]["id"].GetString();

		if (cId == "CONTAINER")
		{
			Container* component = new Container();

			const Value& jsonInventory = jsonComponents[i]["inventory"];

			for (SizeType j = 0; j < jsonInventory.Size(); j++)
			{
				Item item;
				item.loadItem(jsonInventory[j]);
				component->addItem(item);
			}
			this->setComponent(cId, component);
		}
		else if (cId == "CARRYABLE")
		{
			Carryable* component = new Carryable();

			component->encumberment = jsonComponents[i]["encumberment"].GetInt();

			this->setComponent(cId, component);
		}
		else if (cId == "LOCKABLE")
		{
			Lockable* component = new Lockable();

			component->isLocked = jsonComponents[i]["locked"].GetBool();
			component->keyName = jsonComponents[i]["key"].GetString();
			component->desc = jsonComponents[i]["desc"].GetString();

			this->setComponent(cId, component);
		}
	}
}

//int Item::indexByName(std::string itemName)
//{
//	for (unsigned i = 0; i < contains.size(); i++)
//	{
//		if (contains[i].id == itemName)
//		{
//			return i;
//		}
//	}
//	return -1;
//}
//
//void Item::removeItem(int itemIndex)
//{
//	contains.erase(contains.begin() + itemIndex);
//}

bool Item::hasComponent(std::string name)
{
	if (components.find(name) == components.end())
	{
		return false;
	}
	else
	{
		return true;
	}
}

Component* Item::getComponent(std::string name)
{
	Component* c = components[name];
	return c;
}

void Item::setComponent(std::string name, Component* component)
{
	components.insert(make_pair(name, component));
}