#ifndef ITEM_H
#define ITEM_H

#include <algorithm> 
#include <string>
#include <thread>
#include <future>
#include <sstream>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <map>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "Component.h"

using namespace rapidjson;

class Component;

class Item
{
public:
	virtual void loadItem(const Value& jsonItem);
	//virtual int indexByName(std::string itemName);
	//virtual void removeItem(int itemIndex);

	virtual bool hasComponent(std::string name);
	virtual Component* getComponent(std::string name);
	virtual void setComponent(std::string name, Component* component);

	std::string id;
	std::string name;
	std::string desc;
	std::string origin;
	//std::vector<Item> contains;
protected:
	std::map<std::string, Component*> components;
};

#endif