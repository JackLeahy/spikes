#ifndef COMPONENT_H
#define COMPONENT_H

#include <algorithm> 
#include <string>
#include <thread>
#include <future>
#include <sstream>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <map>

using namespace std;

class Item;

class Component
{
public:
	virtual ~Component() {}
};

class Carryable : public Component
{
public:
	int encumberment;
	std::string desc;
};

class Lockable : public Component
{
public:
	virtual void unlock(std::string keyName);
	virtual void lock();
	bool isLocked;
	std::string keyName;
	std::string desc;
};

class Container : public Component
{
public:
	virtual int indexByName(std::string itemName);
	virtual void addItem(Item item);
	virtual void removeItem(int itemIndex);
	virtual void printContents();
	virtual Item getItemAtIndex(int index);
//private:
	std::vector<Item> inventory;
};

#endif