#include <algorithm> 
#include <string>
#include <thread>
#include <future>
#include <sstream>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <map>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "Room.h"
#include "Adventure.h"

using namespace std;
using namespace rapidjson;

const char* parseFile(std::string path)
{
	std::ifstream in(path);
	std::string contents((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());

	const char* json = contents.c_str();
	cout << json;

	return json;
}

Adventure::Adventure(std::string adventureName)
{
	std::string path;
	if (adventureName == "Adventure 1")
	{
		path = "data/ForestCaveAdventure.json";
	}
	else if (adventureName == "Adventure 2")
	{
		path = "data/testadventuremap.json";
	}
	loadAdventure(path);
	movePlayerToRoom(this->startRoom);
}

void Adventure::loadAdventure(std::string path)
{
	std::ifstream in(path);
	std::string contents((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());

	const char* json = contents.c_str();

	Document document;
	document.Parse(json);

	const Value& jsonRooms = document["rooms"];

	for (SizeType i = 0; i < jsonRooms.Size(); i++)
	{
		Room room = Room(jsonRooms[i]);
		rooms.push_back(room);
	}

	const Value& jsonStart = document["startRoom"];
	startRoom = jsonStart.GetInt();
}

void Adventure::pickupItem(std::string itemName)
{
	int itemIndex = rooms[player.currentRoomId].indexByName(itemName);
	Item item;
	if(itemIndex != -1)
	{
		item = rooms[player.currentRoomId].items[itemIndex];
		if (item.hasComponent("CARRYABLE"))
		{
			player.pickupItem(rooms[player.currentRoomId].items[itemIndex]);
			rooms[player.currentRoomId].items.erase(rooms[player.currentRoomId].items.begin() + itemIndex);
			std::cout << "Taken." << endl;
		}
		else
		{
			std::cout << "You cannot carry that item." << endl;
		}
		
	}
	else
	{
		std::cout << "That item isn't here." << endl;
	}
}

void Adventure::dropItem(std::string itemName)
{
	int itemIndex = player.indexByName(itemName);
	if (itemIndex != -1)
	{
		rooms[player.currentRoomId].items.push_back(player.inventory[itemIndex]);
		player.dropItem(itemIndex);
		std::cout << "You drop the " << itemName << "." << endl;
	}
	else
	{
		std::cout << "You aren't holding that item." << endl;
	}
}

void Adventure::lookInItem(std::string itemName)
{
	Container* container;
	Lockable* lockable;
	int itemIndex;

	Item item;

	if (rooms[player.currentRoomId].indexByName(itemName) != -1)
	{
		itemIndex = rooms[player.currentRoomId].indexByName(itemName);
		item = rooms[player.currentRoomId].items[itemIndex];
	}
	else if (player.indexByName(itemName) != -1)
	{
		itemIndex = player.indexByName(itemName);
		item = player.inventory[itemIndex];
	}
	else
	{
		std::cout << "There is no " << itemName << " here." << endl;
	}

	//if container can be locked
	if (item.getComponent("LOCKABLE"))
	{
		lockable = (Lockable *)item.getComponent("LOCKABLE");
		//if container is locked, player cannot access contents
		if (lockable->isLocked)
		{
			std::cout << "The " << itemName << " is locked shut. You will need its key." << endl;
			return;
		}
	}

	if (item.hasComponent("CONTAINER"))
	{
		container = (Container *) item.getComponent("CONTAINER");

		container->printContents();
	}
	else
	{
		std::cout << "You cannot look inside the " << itemName << "." << endl;
	}
}

void Adventure::takeFromContainer(std::string itemName, std::string containerName)
{
	Container* container;
	Lockable* lockable;

	int itemIndex;
	Item* itemContainer;
	Item item;
	//if container is in room
	if (rooms[player.currentRoomId].indexByName(containerName) != -1)
	{
		itemIndex = rooms[player.currentRoomId].indexByName(containerName);
		itemContainer = &rooms[player.currentRoomId].items[itemIndex];
	}
	//if container is in player inventory
	else if(player.indexByName(containerName) != -1)
	{
		itemIndex = player.indexByName(containerName);
		itemContainer = &player.inventory[itemIndex];
	}
	//container is not accessible
	else
	{
		std::cout << "There is no " << itemName << " here." << endl;
		return;
	}
	//if container can be locked
	if (itemContainer->hasComponent("LOCKABLE"))
	{
		lockable = (Lockable *)itemContainer->getComponent("LOCKABLE");
		//if container is locked, player cannot access contents
		if (lockable->isLocked)
		{
			std::cout << "The " << containerName << " is locked shut. You will need its key." << endl;
			return;
		}
	}
	//if container item can contain items
	if (itemContainer->hasComponent("CONTAINER"))
	{
		container = (Container *)itemContainer->getComponent("CONTAINER");
		//if container item contains specified item
		if (container->indexByName(itemName) != -1)
		{
			itemIndex = container->indexByName(itemName);
			item = container->getItemAtIndex(itemIndex);
		}
		//item wasn't inside the container
		else
		{
			std::cout << "There isn't a " << itemName << " inside the " << containerName << "." << endl;
			return;
		}
		container->removeItem(itemIndex);
		player.pickupItem(item);
		std::cout << "Taken." << endl;
	}
	//
	else
	{
		std::cout << "There is nothing inside the " << containerName << "." << endl;
	}
}
//Places item (itemName) inside another item/container (containerName)
void Adventure::putInContainer(std::string itemName, std::string containerName)
{
	Container* container;
	Lockable* lockable;

	Item* itemContainer;
	int	itemIndex = player.indexByName(itemName);

	//if item exists in player inventory
	if (itemIndex != -1)
	{
		//if container exists in rooms
		if (rooms[player.currentRoomId].indexByName(containerName) != -1)
		{
			itemContainer = &rooms[player.currentRoomId].items[rooms[player.currentRoomId].indexByName(containerName)];
		}
		//if container exists in player inventory
		else if(player.indexByName(containerName) != -1)
		{
			itemContainer = &player.inventory[player.indexByName(containerName)];
		}
		else
		{
			std::cout << "There isn't that item here." << endl;
			return;
		}

		//if item is has a lock
		if (itemContainer->hasComponent("LOCKABLE"))
		{
			lockable = (Lockable *)itemContainer->getComponent("LOCKABLE");
			//if item is locked, player cannot acccess container
			if (lockable->isLocked)
			{
				std::cout << "The " << containerName << " is locked shut. You will need its key." << endl;
				return;
			}
		}
		//if item container is a container, 
		if (itemContainer->hasComponent("CONTAINER"))
		{
			container = (Container *)itemContainer->getComponent("CONTAINER");
			container->addItem(player.inventory[itemIndex]);
			player.dropItem(itemIndex);
			std::cout << "You placed the " << itemName << " inside the " << containerName << "." << endl;
		}
		else
		{
			std::cout << "You cannot place anything inside the " << containerName << "." << endl;
		}
	}
	else
	{
		std::cout << "You don't have a " << itemName << "." << endl;
		return;
	}
}

void Adventure::openContainer(std::string itemName, std::string containerName)
{
	Lockable* lockable;
	Item* itemContainer;
	int	itemIndex = player.indexByName(itemName);

	//if item exists in player inventory
	if (itemIndex != -1)
	{
		//if container exists in rooms
		if (rooms[player.currentRoomId].indexByName(containerName) != -1)
		{
			itemContainer = &rooms[player.currentRoomId].items[rooms[player.currentRoomId].indexByName(containerName)];
		}
		//if container exists in player inventory
		else if (player.indexByName(containerName) != -1)
		{
			itemContainer = &player.inventory[player.indexByName(containerName)];
		}
		else
		{
			std::cout << "There isn't that item here." << endl;
			return;
		}

		//if item is has a lock
		if (itemContainer->hasComponent("LOCKABLE"))
		{
			lockable = (Lockable *)itemContainer->getComponent("LOCKABLE");
			//if item is locked, unlock container
			if (lockable->isLocked)
			{
				lockable->unlock(itemName);
				std::cout << lockable->desc << endl;
				return;
			}
			else
			{
				std::cout << "This item is already unlocked." << endl;
			}
		}
		else
		{
			std::cout << "This item cannot be locked or unlocked." << endl;
		}
	}
	else
	{
		std::cout << "You don't have a " << itemName << "." << endl;
		return;
	}
}

void Adventure::printInventory()
{
	if (player.inventory.size() > 0)
	{
		std::cout << "   --|INVENTORY|--  " << endl;
		for (unsigned i = 0; i < player.inventory.size(); i++)
		{
			std::cout << player.inventory[i].name << endl;
		}
	}
	else
	{
		std::cout << "Your inventory is empty." << endl;
	}
}

void Adventure::movePlayerInDir(std::string dir)
{
	if (rooms[player.currentRoomId].hasConnectionInDir(dir))
	{
		movePlayerToRoom(rooms[player.currentRoomId].getRoomConnection(dir));
	}
}

void Adventure::movePlayerToRoom(int roomId)
{
	player.move(roomId);
	rooms[player.currentRoomId].printDesc();
}