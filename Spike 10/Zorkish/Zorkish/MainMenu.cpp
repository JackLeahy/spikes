#include <iostream>
#include <algorithm> 
#include <string>
#include <future>

#include "GameEngine.h"
#include "Utils.h"

using namespace std;

const enum STATES
{
	MAIN_MENU,
	SELECT_ADVENTURE,
	ADVENTURE_ONE,
	ADVENTURE_TWO,
	HELP,
	ABOUT,
	NEW_HIGH_SCORE,
	HALL_OF_FAME,
	QUIT
};

STATES stringToEnum(std::string state)
{
	if (state == "Main Menu") { return MAIN_MENU; }
	else if (state == "Select Adventure") { return SELECT_ADVENTURE; }
	else if (state == "Adventure 1") { return ADVENTURE_ONE; }
	else if (state == "Adventure 2") { return ADVENTURE_TWO; }
	else if (state == "Help") { return HELP; }
	else if (state == "About") { return ABOUT; }
	else if (state == "New High Score") { return NEW_HIGH_SCORE; }
	else if (state == "Hall of Fame") { return HALL_OF_FAME; }
	else if (state == "Back") { return MAIN_MENU; }
	else if (state == "Quit") { return QUIT; }
	else return MAIN_MENU;
}

class StateContext
{
	class State *current;
public:
	void setCurrent(State *s)
	{
		current = s;
	}
	void changeState(STATES state);
};

class State
{
public:
	virtual ~State() {}
	//virtual bool validateInput(string input) = 0;
	virtual void runState(StateContext* sc) = 0;

	string getInput()
	{
		std::cout << prompt;

		std::string input;
		do
		{
			input = getLine();
			input = options[std::stoi(input) - 1];
		} while (!validateInput(input));
		return input;
	}

	void setName(string name)
	{
		this->name = name;
	}

	void printName()
	{
		std::cout << name << endl;
	}

	void setText(string text)
	{
		this->text = text;
	}

	void setPrompt(string prompt)
	{
		this->prompt = prompt;
	}

	void printText()
	{
		std::cout << text << endl;
	}

	void addOption(std::string option)
	{
		options.push_back(option);
	}

	void printOptions()
	{
		std::string text;

		for (unsigned i = 0; i < options.size(); i++)
		{
			text += "[" + std::to_string(i + 1) + "]" + options[i] + "\n";
		}
		std::cout << text;
	}

	bool validateInput(string input)
	{
		return true;
	}

	void changeState(STATES state, StateContext* sc)
	{
		sc->changeState(state);
		delete this;
	}
private:
	std::string name;
	std::string text;
	std::string prompt;
	std::vector<std::string> options;
};

class AdventureOne : public State
{
public:
	AdventureOne()
	{
		setName("Forest Cave Adventure");
		setText("An original adventure by Jack Leahy.");
	}
	void runState(StateContext* sc)
	{
		runGame("Adventure 1");
		changeState(MAIN_MENU, sc);
	}
};

class AdventureTwo : public State
{
public:
	AdventureTwo()
	{
		setName("Adventure 2");
		setText("This is an Example/Test Adventure!");
	}
	void runState(StateContext* sc)
	{
		runGame("Adventure 2");
		changeState(MAIN_MENU, sc);
	}
};

class MainMenu : public State
{
public:
	MainMenu()
	{
		setName("MAIN MENU");
		printName();

		addOption("Select Adventure");
		addOption("Help");
		addOption("About");
		addOption("Hall of Fame");
		addOption("Quit");
		printOptions();

		setPrompt("Enter [#] Selection > ");
	}
	void runState(StateContext* sc)
	{
		STATES state = stringToEnum(getInput());
		lineBreak();
		changeState(state, sc);
	}
};

class SelectAdventure : public State
{
public:
	SelectAdventure()
	{
		setName("SELECT ADVENTURE");
		printName();

		addOption("Adventure 1");
		addOption("Adventure 2");
		addOption("Back");
		addOption("Quit");
		printOptions();

		setPrompt("Enter [#] Selection > ");
	}
	void runState(StateContext* sc)
	{
		STATES state = stringToEnum(getInput());
		lineBreak();
		changeState(state, sc);
	}
};

class Help : public State
{
public:
	Help()
	{
		setName("HELP");
		printName();

		setText("Command Glossary:");
		printText();

		addOption("Back");
		addOption("Quit");
		printOptions();

		setPrompt("Enter [#] Selection > ");
	}
	void runState(StateContext* sc)
	{
		STATES state = stringToEnum(getInput());
		lineBreak();
		changeState(state, sc);
	}
};

class About : public State
{
public:
	About()
	{
		setName("ABOUT");
		printName();

		setText("This version of Zorkish was made by Jack Leahy.");
		printText();

		addOption("Back");
		addOption("Quit");
		printOptions();

		setPrompt("Enter [#] Selection > ");
	}
	void runState(StateContext* sc)
	{
		STATES state = stringToEnum(getInput());
		lineBreak();
		changeState(state, sc);
	}
};

class NewHighScore : public State
{
public:
	NewHighScore()
	{
		setName("NEW HIGH SCORE!");
		printName();

		setText("Congratulations! You beat the previous high score!");
		printText();

		setPrompt("Enter Your Name > ");
	}
	void runState(StateContext* sc)
	{
		STATES state = stringToEnum(getInput());
		lineBreak();
		changeState(state, sc);
	}
};

class HallOfFame : public State
{
public:
	HallOfFame()
	{
		setName("HALL OF FAME");
		printName();

		setText("| SCORE | -- |    NAME    |");
		printText();
		setText("|-------------------------|");
		printText();
		setText("| 09001 | -- |   Vegeta   |");
		printText();
		setText("| 00420 | -- |    Jack    |");
		printText();
		setText("| 00322 | -- |    Solo    |");
		printText();
		setText("|-------| -- |------------|");
		printText();

		addOption("Back");
		addOption("Quit");
		printOptions();

		setPrompt("Enter [#] Selection > ");
	}
	void runState(StateContext* sc)
	{
		STATES state = stringToEnum(getInput());
		lineBreak();
		changeState(state, sc);
	}
};

void StateContext::changeState(STATES state)
{
	switch (state)
	{
		case MAIN_MENU:
		{
			this->setCurrent(new MainMenu());
			break;
		}
		case SELECT_ADVENTURE:
		{
			this->setCurrent(new SelectAdventure());
			break;
		}
		case ADVENTURE_ONE:
		{
			this->setCurrent(new AdventureOne());
			break;
		}
		case ADVENTURE_TWO:
		{
			this->setCurrent(new AdventureTwo());
			break;
		}
		case HELP:
		{
			this->setCurrent(new Help());
			break;
		}
		case ABOUT:
		{
			this->setCurrent(new About());
			break;
		}
		case NEW_HIGH_SCORE:
		{
			this->setCurrent(new NewHighScore());
			break;
		}
		case HALL_OF_FAME:
		{
			this->setCurrent(new HallOfFame());			
			break;
		}
		case QUIT:
		{
			std::cout << "You have QUIT. Thanks for playing!" << endl;
			lineBreak();
			break;
		}
	}

	if (state != QUIT)
	{
		current->runState(this);
	}
};

int main()
{
	std::cout << "Welcome to Zorkish!" << endl;
	lineBreak();

	StateContext stateContext;
	//stateContext.changeState(MAIN_MENU);
	stateContext.changeState(ADVENTURE_ONE);

	return 0;
}
