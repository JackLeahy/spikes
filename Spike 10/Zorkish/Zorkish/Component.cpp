#include "Component.h"
#include "Item.h"

int Container::indexByName(std::string itemName)
{
	for (unsigned i = 0; i < this->inventory.size(); i++)
	{
		if (this->inventory[i].id == itemName)
		{
			return i;
		}
	}
	return -1;
}

void Container::addItem(Item item)
{
	inventory.push_back(item);
}

void Container::removeItem(int itemIndex)
{
	this->inventory.erase(this->inventory.begin() + itemIndex);
}

//unsigned Container::inventorySize()
//{
//	return this->inventory.size();
//}

void Container::printContents()
{
	if (this->inventory.size() > 0)
	{
		for (unsigned i = 0; i < this->inventory.size(); i++)
		{
			std::cout << this->getItemAtIndex(i).origin << endl;
		}
	}
	else
	{
		std::cout << "This item is empty." << endl;
	}
}

Item Container::getItemAtIndex(int index)
{
	return this->inventory[index];
}

void Lockable::unlock(std::string keyName)
{
	if (keyName == this->keyName)
	{
		this->isLocked = false;
	}
	else
	{
		std::cout << "That failed to unlock it.";
	}
}

void Lockable::lock()
{
	this->isLocked = true;
}