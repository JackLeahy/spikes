#include "Room.h"

Room::Room(const Value& jsonRoom)
{
	id = jsonRoom["id"].GetInt();
	name = jsonRoom["name"].GetString();
	desc = jsonRoom["desc"].GetString();

	for (SizeType i = 0; i < jsonRoom["connections"].Size(); i++)
	{
		connections.insert(make_pair(jsonRoom["connections"][i]["dir"].GetString(), jsonRoom["connections"][i]["id"].GetInt()));
	}

	const Value& jsonItems = jsonRoom["items"];

	for (SizeType i = 0; i < jsonItems.Size(); i++)
	{
		Item item;
		item.loadItem(jsonItems[i]);
		items.push_back(item);
	}
}

bool Room::hasConnectionInDir(std::string dir)
{
	if (this->connections.count(dir) != 1)
	{
		std::cout << "You cannot go in that direction." << endl;
		return false;
	}
	return true;
}

int Room::getRoomConnection(std::string dir)
{
	return this->connections[dir];
}

int Room::indexByName(std::string itemName)
{
	for (unsigned i = 0; i < items.size(); i++)
	{
		if (items[i].id == itemName)
		{
			return i;
		}
	}
	return -1;
}