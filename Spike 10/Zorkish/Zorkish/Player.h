#ifndef PLAYER_H
#define PLAYER_H

#include "Item.h"

class Player
{
public:
	Player();
	virtual void move(int roomId);
	virtual void changeScore(int increment);
	virtual void pickupItem(Item item);
	virtual void dropItem(int itemIndex);
	virtual int indexByName(std::string itemName);

	std::vector<Item> inventory;
	int	currentRoomId;
	int score;
};

#endif