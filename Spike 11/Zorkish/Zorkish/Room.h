#ifndef ROOM_H
#define ROOM_H

#include <algorithm> 
#include <string>
#include <future>
#include <sstream>
#include <iostream>
#include <fstream>
#include <map>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "Item.h"
#include "Message.h"

using namespace std;
using namespace rapidjson;

class Room
{
public:
	Room(const Value& jsonRoom, MessageBoard &mb);

	void printDesc()
	{
		std::cout << "  --  " << name << "  --  " << endl;
		std::cout << desc << endl;
		printItems();
	}

	void printItems()
	{
		for (unsigned i = 0; i < items.size(); i++)
		{
			std::cout << items[i].origin << endl;
		}
	}

	void printConnections()
	{
		typedef std::map<std::string, int>::iterator it_type;
		for (it_type iterator = connections.begin(); iterator != connections.end(); iterator++) {
			std::cout << iterator->first << endl;
			std::cout << iterator->second << endl;
		}
	}

	virtual bool hasConnectionInDir(std::string dir);
	virtual int getRoomConnection(std::string dir);
	virtual int indexByName(std::string itemName);
	
	int id;
	std::string name;
	std::string desc;
	std::map<std::string, int> connections;
	std::vector<Item> items;
};

#endif
