#include "Item.h"

void Item::loadItem(const Value& jsonItem, MessageBoard &mb)
{
	this->id = jsonItem["id"].GetString();
	this->name = jsonItem["name"].GetString();
	this->desc = jsonItem["desc"].GetString();
	this->origin = jsonItem["origin"].GetString();

	const Value& jsonComponents = jsonItem["components"];

	for (SizeType i = 0; i < jsonComponents.Size(); i++)
	{
		std::string cId = jsonComponents[i]["id"].GetString();

		if (cId == "CONTAINER")
		{
			Container* component = new Container();

			const Value& jsonInventory = jsonComponents[i]["inventory"];

			for (SizeType j = 0; j < jsonInventory.Size(); j++)
			{
				Item item;
				item.loadItem(jsonInventory[j], mb);
				component->addItem(item);
			}
			this->setComponent(cId, component);
		}
		else if (cId == "CARRYABLE")
		{
			Carryable* component = new Carryable();

			component->encumberment = jsonComponents[i]["encumberment"].GetInt();

			this->setComponent(cId, component);
		}
		else if (cId == "LOCKABLE")
		{
			Lockable* component = new Lockable();

			component->isLocked = jsonComponents[i]["locked"].GetBool();
			component->keyName = jsonComponents[i]["key"].GetString();
			component->desc = jsonComponents[i]["desc"].GetString();

			this->setComponent(cId, component);
		}
	}
	//this->mb = &mb;
	subscribe(mb);
}

void Item::subscribe(MessageBoard &mb)
{
	mb.subscribe(id, this);
}

void Item::receiveMessage(Message message)
{
	if (message.component == "UNLOCK" || message.component == "LOCK")
	{
		if (this->hasComponent("LOCKABLE"))
		{
			Lockable* lockable = (Lockable *)this->getComponent("LOCKABLE");
			if (message.info == "UNLOCK")
			{
				if (lockable->isLocked)
				{
					lockable->unlock(message.info);
				}
				else
				{
					std::cout << "This item is already unlocked." << endl;
				}
			}
			else if (message.info == "LOCK")
			{
				lockable->lock();
			}
		}
		else
		{
			std::cout << "This item cannot be locked or unlocked." << endl;
		}
	}
	else
	{
		cout << "ERROR";
	}
}

void Item::requestFromBlackboard()
{
	//std::vector<Message> messages = mb->requestFromBlackboard(this->id);

	/*for (unsigned i = 0; i < messages.size(); i++)
	{
		this->receiveMessage(messages[i]);
	}*/
}

//int Item::indexByName(std::string itemName)
//{
//	for (unsigned i = 0; i < contains.size(); i++)
//	{
//		if (contains[i].id == itemName)
//		{
//			return i;
//		}
//	}
//	return -1;
//}
//
//void Item::removeItem(int itemIndex)
//{
//	contains.erase(contains.begin() + itemIndex);
//}

bool Item::hasComponent(std::string name)
{
	if (components.find(name) == components.end())
	{
		return false;
	}
	else
	{
		return true;
	}
}

Component* Item::getComponent(std::string name)
{
	Component* c = components[name];
	return c;
}

void Item::setComponent(std::string name, Component* component)
{
	components.insert(make_pair(name, component));
}