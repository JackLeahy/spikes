#include "Message.h"

void MessageBoard::subscribe(std::string id, Item* item)
{
	Subscriber sub = Subscriber();
	sub.subscriberId = id;
	sub.reciever = item;
	subscribers.push_back(sub);
}

void MessageBoard::sendMessage(Message message)
{
	for (unsigned i = 0; i < subscribers.size(); i++)
	{
		if (subscribers[i].subscriberId == message.subscriberId)
		{
			Item* ptr = subscribers[i].reciever;
			//->receiveMessage(message);
		}
	}
}

std::vector<Message> MessageBoard::requestFromBlackboard(std::string id)
{
	std::vector<Message> messages;
	for (unsigned i = 0; i < blackboard.size(); i++)
	{
		if (blackboard[i].subscriberId == id)
		{
			messages.push_back(blackboard[i]);
			if (blackboard[i].deleteAfterRead)
			{
				blackboard.erase(blackboard.begin() + i);
			}
		}
	}
	return messages;
}

void MessageBoard::postToBlackboard(Message message)
{
	blackboard.push_back(message);
}