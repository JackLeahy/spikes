#include <algorithm> 
#include <string>
#include <thread>
#include <future>
#include <sstream>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <map>

#include "Adventure.h"
#include "Commander.h"
#include "Utils.h"

using namespace std;

int runGame(std::string adventureName)
{
	MessageBoard mb = MessageBoard();
	Adventure *adventure = new Adventure(adventureName, mb);
	CommandManager cm = CommandManager(adventure);

	std::string input;
	do
	{
		std::cout << ">";
		
		input = getLine();
		
		cm.recieveCommand(input);
	} while (input != "QUIT");
	
	delete adventure;
	return 0;
}