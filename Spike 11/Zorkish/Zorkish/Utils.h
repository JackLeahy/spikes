#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <iostream>

using namespace std;

void lineBreak();

std::string getLine();

std::vector<std::string> split(std::string str, std::string del);

#endif