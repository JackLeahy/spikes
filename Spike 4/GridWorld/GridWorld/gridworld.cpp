#include <iostream>
#include <algorithm> 
#include <string>
#include <thread>
#include <future>

using namespace std;

const string BLANK = " ";
const string CHAR = "S";
const string WALL = "#";
const string GOAL = "G";
const string DEATH = "D";

const int MAP_WIDTH = 8;
const int MAP_HEIGHT = 7;
const int START_ROW = 7;
const int START_COLUMN = 2;

class Coordinate
{
	public:
		int row;
		int column;
};

class Map
{
	public:
		string map[8][8] =
		{
			{ "#","#","#","#","#","#","#","#" },
			{ "#","G"," ","D","#","D"," ","#" },
			{ "#"," "," "," ","#"," "," ","#" },
			{ "#","#","#"," ","#"," ","D","#" },
			{ "#"," "," "," ","#"," "," ","#" },
			{ "#"," ","#","#","#","#"," ","#" },
			{ "#"," "," "," "," "," "," ","#" },
			{ "#","#","S","#","#","#","#","#" }
		};
		Coordinate playerPos = Coordinate();
		void movePlayer(string dir);
		string checkEndCondition();
};

void Map::movePlayer(string dir)
{
	if (dir == "N")
	{
		playerPos.row -= 1;
	}
	else if (dir == "E")
	{
		playerPos.column += 1;
	}
	else if (dir == "S")
	{
		playerPos.row += 1;
	}
	else if (dir == "W")
	{
		playerPos.column -= 1;
	}
}

string Map::checkEndCondition()
{
	string condition;

	if (map[playerPos.row][playerPos.column] == GOAL)
	{
		return GOAL;
	}
	else if (map[playerPos.row][playerPos.column] == DEATH)
	{
		return DEATH;
	}
	else
	{
		return BLANK;
	}
}

bool canNorth(Map map, Coordinate pos);
bool canEast(Map map, Coordinate pos);
bool canSouth(Map map, Coordinate pos);
bool canWest(Map map, Coordinate pos);

string globalInput = "";
Map map;

class Move
{
	public:
		bool north;
		bool east;
		bool south;
		bool west;
		void init(Map map, Coordinate pos);
};

void Move::init(Map map, Coordinate pos)
{
	north = canNorth(map, pos);
	east = canEast(map, pos);
	south = canSouth(map, pos);
	west = canWest(map, pos);
}

string getMoveMessage(Map map, Coordinate pos)
{

	Move move = Move();
	move.init(map, pos);

	string message = "You can move [ ";

	if (move.north)
	{
		message += "N ";
	}
	if (move.east)
	{
		message += "E ";
	}
	if (move.south)
	{
		message += "S ";
	}
	if (move.west)
	{
		message += "W ";
	}
	message += "] > ";

	return message;
}

string getLine()
{
	string input;

	getline(cin, input);
	std::transform(input.begin(), input.end(), input.begin(), ::toupper);

	return input;
}

bool isValidInput(string input, Map map, Coordinate pos)
{
	Move move = Move();
	move.init(map, pos);

	if (!move.north && input == "N")
	{
		return false;
	}
	if (!move.east && input == "E")
	{
		return false;
	}
	if (!move.south && input == "S")
	{
		return false;
	}
	if (!move.west && input == "W")
	{
		return false;
	}

	return true;
}

void lineBreak()
{
	cout << "\n----------------------------------------------------------\n";
}

void initMessage()
{
	string startMessage = "Welcome to GridWorld : Quantised Excitement. Fate is waiting for You! \nValid commands [N, S, E, W] for direction. [Q] to quit the game.";
	cout << startMessage;
	lineBreak();
}

void inputFunc()
{
	string input;
	do
	{
		do
		{
			cout << getMoveMessage(map, map.playerPos);
			input = getLine();
		} while (!isValidInput(input, map, map.playerPos));

		globalInput = input;

		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	} while (map.checkEndCondition() != GOAL && map.checkEndCondition() != DEATH && globalInput != "Q");
}

int main()
{
	initMessage();

	map = Map();
	map.playerPos.row = START_ROW;
	map.playerPos.column = START_COLUMN;

	std::thread inputThread(inputFunc);

	do
	{
		if (globalInput != "") {
			map.movePlayer(globalInput);

			globalInput = "";
		}
		else {
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
		}
	} while (map.checkEndCondition() != GOAL && map.checkEndCondition() != DEATH && globalInput != "Q");

	inputThread.join();

	string endMessage;

	if (map.checkEndCondition() == GOAL)
	{
		endMessage = "Wow - you have discovered a large chest filled with GOLD coins! \nYOU WIN! \nThanks for playing. There probably won�t be a next time.";
	}
	else if (map.checkEndCondition() == DEATH)
	{
		endMessage = "Arrrrgh... you have fallen down a pit. \nYOU HAVE DIED! \nThanks for playing. Maybe next time.";
	}
	else if (globalInput == "Q")
	{
		endMessage = "You have Quit. \nThanks for playing.";
	}

	endMessage += "\n";
	cout << endMessage;

	return 0;
}

bool canNorth(Map map, Coordinate pos)
{
	if (pos.row > 0 && map.map[pos.row - 1][pos.column] != WALL)
	{
		return true;
	}
	return false;
}

bool canEast(Map map, Coordinate pos)
{
	if (pos.column < MAP_WIDTH && map.map[pos.row][pos.column + 1] != WALL)
	{
		return true;
	}
	return false;
}

bool canSouth(Map map, Coordinate pos)
{
	if (pos.row < MAP_HEIGHT && map.map[pos.row + 1][pos.column] != WALL)
	{
		return true;
	}
	return false;
}

bool canWest(Map map, Coordinate pos)
{
	if (pos.column > 0 && map.map[pos.row][pos.column - 1] != WALL)
	{
		return true;
	}
	return false;
}